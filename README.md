# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* pyweek 31 entry for team [GD](https://pyweek.org/e/GD/)
* [pyweek 31](https://pyweek.org/31/)

### How do I get set up? ###

* clone repo, follow instructions in readme.txt in the entry directory
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
