# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'multiclient.py' is part of pw-31
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division


try:
    # sys.path.insert(0, '../')  # find the lib is only the repository has been cloned
    import sys
    import traceback
    import logging
    from multiprocessing.connection import Client, wait
    from queue import Queue, Empty
    from threading import Thread
    import psutil

    import messages

    logging.basicConfig()

    logger = logging.getLogger(__name__)

    __author__ = "DR0ID"
    __email__ = "dr0iddr0id {at} gmail [dot] com"
    __copyright__ = "DR0ID @ 2021"
    __credits__ = ["DR0ID"]  # list of contributors
    __maintainer__ = "DR0ID"
    __license__ = "New BSD license"

    q = Queue()


    def get_input():
        while True:
            msg = input("<< ")
            q.put_nowait(msg)


    def main(args=None):
        """
        The main  function.

        :param args: Argument list to use. This is a list of strings like sys.argv. See -h for complete argument list.
            Default=None so the sys.args are used.
        """

        t = Thread(target=get_input)
        t.daemon = True
        t.start()

        running = True
        address = ('localhost', 0x4B1D)
        with Client(address, authkey=b'abc') as client:

            proc = psutil.Process()

            msg = f"PID: {proc.pid}, parent: {proc.parent().pid}"
            client.send(messages.Message(msg))

            while running:
                try:
                    data_input = q.get_nowait()
                    if data_input == 'end':
                        running = False
                    elif data_input == 'quit':
                        data_input = messages.QuitServer()
                    elif data_input == 'qclients':
                        data_input = messages.EndAllClients()
                    else:
                        data_input = messages.Message(data_input)
                    client.send(data_input)
                except Empty:
                    pass

                try:
                    if not client.closed:
                        data_connections = wait([client], 0.1)
                        for data_connection in data_connections:
                            data = data_connection.recv()
                            if isinstance(data, messages.Message):
                                print(">> ", data.msg)
                            elif isinstance(data, messages.QuitClient):
                                running = False

                except EOFError as e:
                    logger.error(e)
                    client.close()
                except Exception as ee:
                    logger.error(ee)
                    client.close()


    if __name__ == '__main__':  # pragma: no cover
        main()

except Exception as ex:
    # trying to catch any exceptions and print them out in a way that the user will see them!
    sys.stderr.write("Error:")
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    sys.stderr.write(str(ex))
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_tb(exc_traceback, file=sys.stderr)
    sys.stderr.write('\n')
    sys.stderr.write('Press any key... ')
    sys.stderr.flush()
    # sys.stdin.readline()
    logging.shutdown()
