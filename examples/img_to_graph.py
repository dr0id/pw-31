# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'img_to_graph.py.py' is part of pw-31
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import json
from typing import Any

try:
    import sys
    import traceback
    import logging

    logging.basicConfig()
    import pygame

    logger = logging.getLogger(__name__)

    __version__ = '1.0.0.0'

    # for easy comparison as in sys.version_info but digits only
    __version_info__ = tuple([int(_d) for _d in __version__.split('.')])

    __author__ = "DR0ID"
    __email__ = "dr0iddr0id {at} gmail [dot] com"
    __copyright__ = "DR0ID @ 2021"
    __credits__ = ["DR0ID"]  # list of contributors
    __maintainer__ = "DR0ID"
    __license__ = "New BSD license"

    __all__ = []  # list of public visible parts of this module 

    SCREENSIZE = 800, 600

    dir_x = pygame.Vector2(1, 0)
    dir_nx = pygame.Vector2(-1, 0)
    dir_y = pygame.Vector2(0, 1)
    dir_ny = pygame.Vector2(0, -1)


    def get_directions(p, data):
        directions = []
        if is_path(data, p + dir_x):
            directions.append(dir_x)
        if is_path(data, p + dir_nx):
            directions.append(dir_nx)
        if is_path(data, p + dir_y):
            directions.append(dir_y)
        if is_path(data, p + dir_ny):
            directions.append(dir_ny)

        return directions


    def is_path(data, p):
        img, iw, ih = data
        if p.x < 0 or p.x >= iw:
            return False

        if p.y < 0 or p.y >= ih:
            return False

        color = img.get_at((int(p.x), int(p.y)))
        if color != (255, 255, 255):
            return True
        return False


    # def find_start(data):
    #     img, iw, ih = data
    #     for x in range(iw):
    #         for y in range(ih):
    #             p = pygame.Vector2(x, y)
    #             if is_path(data, p):
    #                 if get_directions(p, data):
    #                     return p

    def find_vertices(data):
        vertices = []
        img, iw, ih = data
        for y in range(ih):
            pygame.event.pump()
            print(f"finding vertices line {y}/{ih}")
            for x in range(iw):
                p = pygame.Vector2(x, y)
                if is_vertex(data, p, vertices):
                    vertices.append(p)
        return vertices


    def is_vertex(data, p, vertices):
        if is_path(data, p):
            u = is_path(data, p + dir_ny)
            d = is_path(data, p + dir_y)
            l = is_path(data, p + dir_nx)
            r = is_path(data, p + dir_x)
            if (l and u) or \
                    (u and r) or \
                    (r and d) or \
                    (d and l) or \
                    (not d and not u and not r) or \
                    (not d and not u and not l) or \
                    (not l and not r and not u) or \
                    (not l and not r and not d) \
                    :
                return True
        return False


    def find_edges(data, vertices):
        edges = []
        for idx, v in enumerate(vertices):
            directions = get_directions(v, data)
            for direction in directions:
                p = pygame.Vector2(v)
                p += direction
                while not p in vertices and is_path(data, p):
                    p += direction
                edges.append((idx, vertices.index(p), (p - v).length()))

        return edges


    class VectorJSONEncoder(json.JSONEncoder):
        # def __init__(self, *, skipkeys=False, ensure_ascii=True,
        #     check_circular=True, allow_nan=True, sort_keys=False,
        #     indent=None, separators=None, default=None):
        #     super().__init__( skipkeys, ensure_ascii,
        #     check_circular, allow_nan, sort_keys,
        #     indent, separators, default)

        def default(self, o: Any) -> Any:
            if isinstance(o, pygame.Vector2) or isinstance(o, pygame.Vector3):
                return tuple(o)
            return super().default(o)


    import igraph as ig


    def main():
        pygame.init()
        # screen = pygame.display.set_mode(SCREENSIZE, pygame.RESIZABLE | pygame.SRCALPHA, 32)
        screen = pygame.display.set_mode(SCREENSIZE)

        filename = "source2.png"
        image = pygame.image.load(filename)
        iw, ih = image.get_size()
        data = (image, iw, ih)

        # parse the image
        vertices = find_vertices(data)
        edges = find_edges(data, vertices)

        print("vertices", len(vertices))
        print("edges", len(edges))
        g_data = {"vertices": vertices, "edges": edges}

        # save to file
        g_data_file_name = filename + ".gdata.json"
        with open(g_data_file_name, 'w') as f:
            json.dump(g_data, f, cls=VectorJSONEncoder)

        # load from file
        gg_data = None
        with open(g_data_file_name, 'r') as f:
            gg_data = json.load(f)

        try:
            g = create_graph(vertices, edges)

            # Choose the layout
            ig.plot(g)
        except Exception as e:
            print(e)

        # running = True
        # while running:
        #     # events
        #     for event in pygame.event.get():
        #
        #         if event.type == pygame.QUIT:
        #             running = False
        #         elif event.type == pygame.KEYDOWN:
        #             if event.key == pygame.K_ESCAPE:
        #                 running = False
        #
        #     # draw
        #
        #     pygame.display.flip()
        #     screen.fill((255, 0, 255))

        pygame.quit()


    def create_graph(vertices, edges):
        g = ig.Graph(len(vertices), directed=True)
        g.vs["x"] = [_v.x for _v in vertices]  # for plotting
        g.vs["y"] = [_v.y for _v in vertices]  # for plotting
        g.vs["position"] = [tuple(_v) for _v in vertices]  # to use in game
        g.add_edges([(s, t) for s, t, w in edges])
        g.es["weight"] = [w for s, t, w in edges]
        g.es["label"] = [w for s, t, w in edges]  # for plotting
        return g


    if __name__ == '__main__':
        main()

except Exception as ex:
    # trying to catch any exceptions and print them out in a way that the user will see them!
    sys.stderr.write("Error:")
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    sys.stderr.write(str(ex))
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_tb(exc_traceback, file=sys.stderr)
    sys.stderr.write('\n')
    sys.stderr.write('Press any key... ')
    sys.stderr.flush()
    sys.stdin.readline()
    logging.shutdown()
