# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'multimain.py' is part of pw-31
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

try:
    # sys.path.insert(0, '../')  # find the lib is only the repository has been cloned
    import sys
    import traceback
    import logging
    import queue
    import multiprocessing
    from threading import Thread
    from multiprocessing.connection import Listener
    import subprocess

    import messages

    logging.basicConfig()

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    __author__ = "DR0ID"
    __email__ = "dr0iddr0id {at} gmail [dot] com"
    __copyright__ = "DR0ID @ 2021"
    __credits__ = ["DR0ID"]  # list of contributors
    __maintainer__ = "DR0ID"
    __license__ = "New BSD license"

    new_connections = queue.Queue()




    def accept_connections(listener):
        while True:
            try:
                conn = listener.accept()
                logger.info(f"accepted connection {conn}")
                new_connections.put(conn)
            except Exception as caught_exception:
                logger.error(caught_exception)


    def main(args=None):
        """
        The main  function.

        :param args: Argument list to use. This is a list of strings like sys.argv. See -h for complete argument list.
            Default=None so the sys.args are used.
        """
        logger.info("started server")

        connections = {}
        address = ('localhost', 0x4B1D)
        listener = Listener(address, authkey=b'abc', backlog=1)
        accept_thread = Thread(target=accept_connections, name="accept_thread", args=(listener, ))
        accept_thread.daemon = True  # don't worry about stopping... should be done automatically
        accept_thread.start()


        logger.info("start clients")
        # subprocess.run([sys.executable, "multiclient.py"], shell=True,
        #                stdin=None, input=None, stdout=None, stderr=None, capture_output=False, cwd=None,
        #                timeout=None, check=False, encoding=None, errors=None, text=None, env=None,
        #                universal_newlines=None,
        #                creationflags=subprocess.DETACHED_PROCESS)
        creationflags = subprocess.DETACHED_PROCESS | subprocess.CREATE_NEW_PROCESS_GROUP
        # creationflags = subprocess.CREATE_NEW_CONSOLE
        subprocess.Popen([sys.executable, "multiclient.py"], shell=False, close_fds=True, creationflags=creationflags)
        # subprocess.Popen([sys.executable, "multiclient.py"], shell=False, creationflags=subprocess.CREATE_NEW_CONSOLE)
        # subprocess.Popen([sys.executable, "multiclient.py"], shell=False, creationflags=subprocess.CREATE_NEW_CONSOLE)
        # subprocess.Popen([sys.executable, "multiclient.py"], shell=False, creationflags=subprocess.CREATE_NEW_CONSOLE)

        logger.info("clients started")


        running = True
        while running:
            try:
                conn = new_connections.get_nowait()
                connections.setdefault(conn, None)
            except queue.Empty:
                pass

            connections_with_data = multiprocessing.connection.wait(connections.keys(), 0.1)
            for current_connection in connections_with_data:
                try:
                    msg = current_connection.recv()
                    if isinstance(msg, messages.QuitServer):
                        running = False
                    elif isinstance(msg, messages.Message):
                        print("Message: ", msg.msg)
                    elif isinstance(msg, messages.EndAllClients):
                        for c in connections.keys():
                            try:
                                c.send(messages.QuitClient())
                            except Exception as e:
                                logger.warning(f"Could not send quit client to {c}: {e}")
                except EOFError:
                    logger.warning(f"read connection closed {current_connection}")
                    if current_connection in connections:
                        current_connection.close()
                        del connections[current_connection]
                    continue

                try:
                    for c in connections.keys():
                        if c != current_connection:
                            c.send(messages.Message(msg))
                except EOFError:
                    logger.warning(f"send connection closed {c}")
                    if conn in connections:
                        c.close()
                        del connections[c]


        listener.close()


    if __name__ == '__main__':  # pragma: no cover
        main()

except Exception as ex:
    # trying to catch any exceptions and print them out in a way that the user will see them!
    sys.stderr.write("Error:")
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    sys.stderr.write(str(ex))
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_tb(exc_traceback, file=sys.stderr)
    sys.stderr.write('\n')
    sys.stderr.write('Press any key... ')
    sys.stderr.flush()
    sys.stdin.readline()
    logging.shutdown()
