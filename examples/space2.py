# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'space2.py' is part of pw-31
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Small example about ai complaining about space. Using speech to text and text to speech libs.


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

try:
    import sys
    import traceback
    import logging

    logging.basicConfig()
    import pygame
    import pyttsx3

    logger = logging.getLogger(__name__)

    __version__ = '1.0.0.0'

    # for easy comparison as in sys.version_info but digits only
    __version_info__ = tuple([int(_d) for _d in __version__.split('.')])

    __author__ = "DR0ID"
    __email__ = "dr0iddr0id {at} gmail [dot] com"
    __copyright__ = "DR0ID @ 2021"
    __credits__ = ["DR0ID"]  # list of contributors
    __maintainer__ = "DR0ID"
    __license__ = "New BSD license"

    __all__ = []  # list of public visible parts of this module 

    SCREENSIZE = 500, 50


    class Ball(object):

        def __init__(self):
            self.position = pygame.Vector2(0, 0)
            self.direction = pygame.Vector2(1, 1)

        def update(self, current_size):
            self.position += self.direction
            if self.position.x >= current_size[0] or self.position.x <= 0:
                self.direction.x *= -1
                self.position += self.direction
            if self.position.y >= current_size[1] or self.position.y <= 0:
                self.direction.y *= -1
                self.position += self.direction

            if self.position.x >= current_size[0] or self.position.y >= current_size[1]:
                self.position.update(0, 0)


    class Text(object):

        def __init__(self, text, volume=0.5, rate=200.0, voice=None):
            self.voice = voice
            self.rate = rate
            self.volume = volume
            self.text = text

        def say(self, engine, name):
            print("say", self.text)
            engine.setProperty("rate", self.rate)
            engine.setProperty("volume", self.volume)
            if self.voice:
                engine.setProperty("voice", self.voice)
            engine.say(self.text, name)


    class Talker(object):

        def __init__(self):
            self.space_text = [Text("Thank you! Now I got space! Almost too much space.")]
            self.compressed_text = [Text("Hello! Help! Help!", volume=1.0),
                                    Text("Hello? I'm in a tight place.", volume=1.0, rate=100),
                                    Text("I feel I have no space...", volume=0.25, rate=50),
                                    Text("Can you help me please?"),
                                    Text("Hello!"),
                                    Text("Its so tight! Please resize the window.")]
            self.texts = self.compressed_text
            self.next_time = -1
            self.time = 0
            self.has_space = False

        def update(self, dt, engine, space):
            self.time += dt
            # print(">>>>>>>>>>>>>>>>>>update", self.time, self.next_time)
            w, h = space
            if w > 200 and h > 200:
                if not self.has_space:
                    print(">>>>>>>>>>>>>>>>>>ENOUGH SPACE")
                    self.has_space = True
                    engine.stop()
                    t = Text("Yesss!")
                    t.say(engine, "relax")
                    self.next_time = self.time
                    self.texts = self.space_text
            else:
                if self.has_space:
                    print(">>>>>>>>>>>>>>>>>>TOO TIGHT")
                    self.has_space = False
                    self.next_time = self.time
                    engine.stop()
                    t = Text("Nooooo!", 1.0, 5)
                    t.say(engine, "exclaim")
                    self.texts = self.compressed_text

            if self.time >= self.next_time:
                self.next_time += 5
                print(">>>>>>>>>>>>>>>>>>update", self.time, self.next_time)
            text = self.texts.pop(0)
            self.texts.append(text)
            text.say(engine, "talker")


    def main():

        def onStart(name):
            print('starting', name)

        def onWord(name, location, length, *args, **kwargs):
            print('word', name, location, length, args, kwargs)

        def onEnd(name, completed):
            print('finishing', name, completed)
            # if name == 'fox':
            #     engine.say('What a lazy dog!', 'dog')
            # elif name == 'dog':
            #     engine.endLoop()

        engine = pyttsx3.init(debug=True)
        engine.connect('started-utterance', onStart)
        engine.connect('started-word', onWord)
        engine.connect('finished-utterance', onEnd)
        print(engine.getProperty("rate"))
        print(engine.getProperty("volume"))
        print(engine.getProperty("voice"))

        # engine.say('The quick brown fox jumped over the lazy dog.', 'fox')
        engine.startLoop(False)

        pygame.init()
        # screen = pygame.display.set_mode(SCREENSIZE, pygame.RESIZABLE | pygame.SRCALPHA, 32)
        screen = pygame.display.set_mode(SCREENSIZE, pygame.RESIZABLE)
        current_size = SCREENSIZE

        clock = pygame.time.Clock()
        ball = Ball()
        talker = Talker()

        running = True
        while running:
            dt = clock.tick() / 1000.0  # convert to seconds
            engine.iterate()
            # events
            for event in pygame.event.get():

                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        running = False
                elif event.type == pygame.VIDEORESIZE:
                    print(event)
                    current_size = event.size

            # update
            ball.update(current_size)
            talker.update(dt, engine, current_size)

            # draw
            pygame.draw.circle(screen, (255, 255, 255), ball.position, 10)
            pygame.display.flip()
            screen.fill((255, 0, 255))

        pygame.quit()
        engine.endLoop()


    if __name__ == '__main__':
        main()

except Exception as ex:
    # trying to catch any exceptions and print them out in a way that the user will see them!
    sys.stderr.write("Error:")
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    sys.stderr.write(str(ex))
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_tb(exc_traceback, file=sys.stderr)
    sys.stderr.write('\n')
    sys.stderr.write('Press any key... ')
    sys.stderr.flush()
    sys.stdin.readline()
    logging.shutdown()
