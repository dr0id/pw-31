# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'messages.py' is part of pw-31
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Shared module because all data structures that are passed between processes need to be known in all processes.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2021"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []    # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class BaseMessage(object):
    def __init__(self):
        super().__init__()
        self.destination = None
        self.sender = None

class ActivateTextInput(BaseMessage): pass

class SpeechEnded(BaseMessage):
    def __init__(self, speech_id, completed):
        super().__init__()
        self.speech_id = speech_id
        self.completed = completed


class Say(BaseMessage):
    def __init__(self, text, rate=175, volume=0.5, interrupt=False, voice=None, speech_id=None, append_input=False):
        super().__init__()
        self.destination = 'tts'
        self.interrupt = interrupt
        self.text = text
        self.rate = rate
        self.volume = volume
        self.voice = voice
        self.speech_id = speech_id
        self.append_input = append_input

    def clone(self):
        return Say(self.text, self.rate, self.volume, self.interrupt, self.voice, self.speech_id, self.append_input)


class PartialSpeechRecorded(BaseMessage):
    def __init__(self, text):
        super().__init__()
        self.text = text


class SpeechRecorded(BaseMessage):
    def __init__(self, text):
        super().__init__()
        self.text = text


class Quit(BaseMessage): pass


class QuitWinEnd(BaseMessage): pass


class QuitLoose1(BaseMessage): pass


class QuitLoose2(BaseMessage): pass


class QuitProcess(BaseMessage):
    def __init__(self, proc_name):
        super().__init__()
        self.proc_name = proc_name


class StartLevel(BaseMessage):
    def __init__(self, level_nr):
        super().__init__()
        self.level_nr = level_nr


class BaseCmd(BaseMessage): pass


class BaseCmdWithText(BaseCmd):

    def __init__(self, text):
        super().__init__()
        self.text = text


class CmdUp(BaseCmdWithText):
    def __init__(self, text):
        super().__init__(text)


class CmdDown(BaseCmdWithText):
    def __init__(self, text):
        super().__init__(text)


class CmdLeft(BaseCmdWithText):
    def __init__(self, text):
        super().__init__(text)


class CmdRight(BaseCmdWithText):
    def __init__(self, text):
        super().__init__(text)


class CmdRepair(BaseCmdWithText):
    def __init__(self, text):
        super().__init__(text)


class CmdClose(BaseCmdWithText):
    def __init__(self, text):
        super().__init__(text)


class CmdOpen(BaseCmdWithText):
    def __init__(self, text):
        super().__init__(text)


class CmdPowerDown(BaseCmdWithText):
    def __init__(self):
        super().__init__("power down")


class HelpAiSayIntro(BaseMessage):
    def __init__(self, speech_id=None):
        super().__init__()
        self.speech_id = speech_id


class HelpAiActivated(BaseMessage): pass


class HelpAiDeactivated(BaseMessage): pass


class CmdHelp(BaseCmdWithText):
    def __init__(self, available_commands: [str]):
        super().__init__("help")
        self.available_commands = available_commands


class MouseFocusChanged(BaseMessage):
    def __init__(self, gain):
        super().__init__()
        self.gain = gain


class KeyboardFocusChanged(BaseMessage):
    def __init__(self, gain):
        super().__init__()
        self.gain = gain


class AppVisible(BaseMessage):
    def __init__(self, gain):
        super().__init__()
        self.gain = gain


logger.debug("imported")
