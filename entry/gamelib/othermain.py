# -*- coding: utf-8 -*-
"""
This it the main module. This is the main entry point of your code (put in the main() method).
"""
from __future__ import division, print_function

import logging
import os

import pygame

from gamelib import messages
from gamelib.shared import Settings, get_messages
from pyknic import context
from pyknic.pyknic_pygame.context import PygameInitContext, AppInitContext, TimeSteppedContext
from pyknic.pyknic_pygame.sfx import MusicPlayer

# from gamelib import settings
# from gamelib.context_gameplay import GameplayContext
# from gamelib.context_load_resources import ResourceLoaderContext, MapResourceConfig
# from gamelib.gamelogic.gamelogic import GameLogic
# # from gamelib.client.client import Client
# # from gamelib.splash_context import SplashContext
# # from gamelib.intro_context import IntroContext
# # from gamelib.victory_context import VictoryContext
# from gamelib.settings import cell_size
# from pyknic import context
# from pyknic.events import EventDispatcher
# from pyknic.mathematics.geometry import SphereBinSpace2
# from pyknic.pyknic_pygame.context import PygameInitContext, AppInitContext
# from pyknic.pyknic_pygame.resource2 import ResizeTransform
# from pyknic.pyknic_pygame.sfx import MusicPlayer
# from pyknic.resource2 import FakeImage, Image, DirectoryAnimation

logger = logging.getLogger(__name__)

settings = Settings()
settings.title = "The OTHER game."  # todo change!?
settings.screen_width = 100
settings.screen_height = 100
settings.screen_flags = pygame.RESIZABLE


class ShowContext(TimeSteppedContext):

    def __init__(self, publisher_queue, consumer_queue, proc_name):
        super().__init__(settings.max_dt, settings.step_dt, settings.draw_fps)
        self.consumer_queue = consumer_queue
        self.publisher_queue = publisher_queue
        self.proc_name = proc_name
        self.position = pygame.Vector2(0, 0)
        self.v = pygame.Vector2(1, 1.234)
        self.screen = None

    def enter(self):
        self.screen = pygame.display.get_surface()

    def exit(self):
        pass

    def update_step(self, delta_time, sim_time, *args):
        self.position += self.v * delta_time
        if not self.screen.get_rect().collidepoint(self.position):
            self.v *= -1
            self.position += 2 * self.v * delta_time
        for event in pygame.event.get():
            if event.type == pygame.VIDEORESIZE:
                pass
        for message in get_messages(self.consumer_queue):
            if isinstance(message, messages.Quit):
                self.pop(100)

    def draw(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0):
        self.screen.fill((0, 0, 0))
        pygame.draw.circle(self.screen, (255, 255, 255), self.position, 20)
        pygame.display.flip()

    def resume(self):
        pass

    def suspend(self):
        pass


def main(log_queue, log_level, publisher_queue, consumer_queue, proc_name, *args):
    # put here your code
    try:
        os.environ["SDL_VIDEO_CENTERED"] = str(1)
        import pyknic
        context.push(PygameInitContext(_custom_display_init))
        music_player = MusicPlayer(settings.master_volume, settings.music_ended_pygame_event)
        context.push(AppInitContext(music_player, settings.path_to_icon, settings.title))

        #
        # # splashscreen = SplashContext()
        # # introscreen = IntroContext()
        # # victoryscreen = VictoryContext()
        #
        # # context.push(victoryscreen)
        #
        # context.push(GameplayContext(GameLogic()))
        # resizeTrans = ResizeTransform(30, 30)
        # resource_def = {
        #     settings.resource_hero: FakeImage(25, 50, (255, 0, 0)),
        #     settings.resource_worm: FakeImage(50, 50, (255, 0, 255)),
        #     settings.resource_map1: MapResourceConfig(
        #         os.path.join(settings.map_dir, settings.map_file_names[settings.current_map])),
        #     settings.resource_cursor: Image("./data/graphics/cursor.png"),
        #     settings.resource_target: Image("./data/graphics/target.png"),
        #     settings.resource_clue_blink: Image("./data/graphics/particlePack/star_08.png"),
        #     # settings.resource_black_smoke: DirectoryAnimation("./data/graphics/smokeparticleassets/blacksmoke", 10, transformation=resizeTrans),
        #     # settings.resource_fart: DirectoryAnimation("./data/graphics/smokeparticleassets/fart", 10, transformation=resizeTrans),
        #     # settings.resource_white_smoke: DirectoryAnimation("./data/graphics/smokeparticleassets/WhitePuff", 10, transformation=resizeTrans),
        # }
        # context.push(ResourceLoaderContext(resource_def))

        context.push(ShowContext(publisher_queue, consumer_queue, proc_name))

        context.set_deferred_mode(True)
        while context.length():
            top = context.top()
            if top:
                top.update(0, 0)
            context.update()  # handle deferred context operations

    except Exception as ex:
        logger.error(ex)
    logger.debug('Finished. Exiting.')


def _custom_display_init(display_info, driver_info, wm_info):
    accommodated_height = int(display_info.current_h * 0.9)
    if accommodated_height < settings.screen_height:
        logger.info("accommodating height to {0}", accommodated_height)
        settings.screen_height = accommodated_height  # accommodate for title bar and task bar ?
    settings.screen_size = settings.screen_width, settings.screen_height
    logger.info("using screen size: {0}", settings.screen_size)

    # initialize the screen
    settings.screen = pygame.display.set_mode(settings.screen_size, settings.screen_flags)

    # mixer values to return
    frequency = settings.MIXER_FREQUENCY
    channels = settings.MIXER_CHANNELS
    mixer_size = settings.MIXER_SIZE
    buffer_size = settings.MIXER_BUFFER_SIZE
    # pygame.mixer.pre_init(frequency, mixer_size, channels, buffer_size)
    return frequency, mixer_size, channels, buffer_size

# this is needed in order to work with py2exe
# if __name__ == '__main__':
#     main()
