# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'tts.py' is part of pw-31
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Speech to text recording module.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import json
import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2021"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


def run(log_queue, log_level, publisher_queue, consumer_queue, proc_name, *args):
    try:
        import os
        import queue
        import zipfile

        import vosk

        from gamelib.shared import get_messages, send_message
        import gamelib.messages as m
        stt_ignore_list = ['huh']

        import sounddevice as sd

        def unzip_model(path_to_zip_file, directory_to_extract_to):
            extracted_directory = os.path.join(directory_to_extract_to,
                                               os.path.splitext(os.path.split(path_to_zip_file)[1])[0])
            if not os.path.exists(extracted_directory):
                logger.info(f"unzipping '{path_to_zip_file}' to directory '{directory_to_extract_to}'")
                with zipfile.ZipFile(path_to_zip_file, 'r') as zip_ref:
                    zip_ref.extractall(directory_to_extract_to)
                logger.info(f"unzipping done")
            else:
                logger.info(f"directory '{directory_to_extract_to}' already exists, nothing to extract!")

        q = queue.Queue()

        def int_or_str(text):
            """Helper function for argument parsing."""
            try:
                return int(text)
            except ValueError:
                return text

        def callback(indata, frames, time, status):
            """This is called (from a separate thread) for each audio block."""
            if status:
                logger.error(status)
            q.put(bytes(indata))

        model_directory = "data/stt/vosk-model-small-en-us-0.15"
        path_to_zip = "data/stt/vosk-model-small-en-us-0.15.zip"
        unzip_model(path_to_zip, "./data/stt")

        model = vosk.Model(model_directory)

        device = None
        device_info = sd.query_devices(device, 'input')

        logger.info(f"Sound device info: {device_info}")
        # soundfile expects an int, sounddevice provides a float:
        sample_rate = int(device_info['default_samplerate'])

        block_size = 8000
        dtype = 'int16'
        channels = 1
        logger.info(
            f"Sound device RawInputStream params: samplerate={sample_rate}, blocksize={block_size}, device={device}, dtype={dtype}, channels={channels}")
        with sd.RawInputStream(samplerate=sample_rate, blocksize=block_size, device=device, dtype=dtype,
                               channels=channels, callback=callback):
            logger.info("recording started")

            rec = vosk.KaldiRecognizer(model, sample_rate)
            json_loads = json.loads

            running = True
            while running:
                try:
                    data = q.get(True, 0.1)
                    if rec.AcceptWaveform(data):
                        rec_result = rec.Result()
                        result_dict = json_loads(rec_result)
                        text_ = result_dict['text']
                        if not text_ or text_.isspace():
                            continue
                        logger.info("recorded: %s", text_)
                        if text_ not in stt_ignore_list:
                            send_message(publisher_queue, m.SpeechRecorded(text_), proc_name)
                            # send_message(publisher_queue, m.Say(text_), name)  # enable this line to have an echo service
                        else:
                            logger.debug(f'stt ignoring: {text_}')
                    else:
                        rec_partial_result = rec.PartialResult()
                        partial_result_dict = json_loads(rec_partial_result)
                        partial_text_ = partial_result_dict['partial']
                        if not partial_text_ or partial_text_.isspace():
                            continue
                        logger.info("partial: %s", partial_text_)
                        send_message(publisher_queue, m.PartialSpeechRecorded(partial_text_), proc_name)
                except queue.Empty:
                    pass

                messages = get_messages(consumer_queue, 0.05)
                for msg in messages:
                    if isinstance(msg, m.Quit):
                        running = False
        logger.info("recording terminated")
    except Exception as ex:
        logger.error(f"speech to text error, TEXT INPUT ONLY MODE! {os.linesep}{ex}")
        send_message(publisher_queue, m.ActivateTextInput(), proc_name)
        send_message(publisher_queue, m.QuitProcess(proc_name), proc_name)

        # this part is to ensure that the message have been sent to the main process
        running = True
        while running:
            messages = get_messages(consumer_queue, 0.05)
            for msg in messages:
                if isinstance(msg, m.Quit):
                    running = False


logger.debug("imported")
