# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'shared.py' is part of pw-31
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Shared helper methods useful in any process.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, annotations

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2021"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []    # list of public visible parts of this module
import queue
import random
import traceback
from logging.handlers import QueueHandler

import pygame

from gamelib import messages
from gamelib.messages import BaseMessage

logger = logging.getLogger(__name__)
logger.debug("importing...")


def send_message(publisher_queue, msg, proc_name):
    assert isinstance(msg, BaseMessage), "msg should be BaseMessage"  # ensure this can be processed
    msg.sender = proc_name
    publisher_queue.put(msg, block=False)


def get_messages(consumer_queue, timeout=None, count=10):
    _messages = []
    try:
        msg = consumer_queue.get(True if timeout else False, timeout)
        assert isinstance(msg, BaseMessage), "Inherit from BaseMessage!"  # ensure that we can process this
        assert msg.sender is not None, f"sender should be set"
        _messages.append(msg)
        while not consumer_queue.empty() and len(_messages) < count:
            msg = consumer_queue.get(False, None)
            assert isinstance(msg, BaseMessage), "Inherit from BaseMessage!"  # ensure that we can process this
            assert msg.sender is not None, f"sender should be set"
            _messages.append(msg)
    except queue.Empty:
        pass
    except Exception as ex:
        logger.error(ex)
    return _messages


def run_wrapped(target, log_queue, log_level, publisher_queue, consumer_queue, name, *args):
    queue_handler = QueueHandler(log_queue)
    try:
        logging.basicConfig(level=log_level if log_level >= logging.INFO else logging.INFO)
        logging.getLogger().addHandler(queue_handler)
        logger.info("args %s", args)

        target(log_queue, log_level, publisher_queue, consumer_queue, name, *args)

    except Exception as ex:
        # print("FATAL: reader({0}) exited while multiprocessing\n{1}".format(args, ex))
        # traceback.print_exc()
        logger.error("FATAL: reader(%s) exited while multiprocessing\n%s".format(args, ex), exc_info=True)
    finally:
        queue_handler.flush()
        queue_handler.close()


class Settings(object):
    def __init__(self):
        self.step_dt = 1.0 / 120.0
        self.max_dt = 20 * self.step_dt
        self.draw_fps = 60
        self.screen_flags = 0
        self.title = "The Cops Game!"
        self.path_to_icon = "./data/icon.png"
        self.music_ended_pygame_event = pygame.USEREVENT
        self.master_volume = 0.5
        self.screen_width = 800
        self.screen_height = 600
        self.MIXER_FREQUENCY = 0  # default:22050
        self.MIXER_SIZE = -16  # default: -16
        self.MIXER_CHANNELS = 2  # default: 2 stereo or 1 mono
        self.MIXER_BUFFER_SIZE = 16  # default: 4096


class BaseResponseEvaluator(object):

    def __init__(self, text):
        self.text = text

    def evaluate(self, speech_record: messages.SpeechRecorded, publish_queue, proc_name):
        pass


class AnyWordEvaluator(BaseResponseEvaluator):

    def __init__(self, any_word_of_list: list):
        super().__init__(", ".join(any_word_of_list))
        self.any_word = any_word_of_list

    def evaluate(self, speech_record: messages.SpeechRecorded, publish_queue, proc_name):
        words = speech_record.text.split()  # this already trims whitespace
        for expected in self.any_word:
            for word in words:
                if word == expected:
                    return word
        return None


class SentenceEvaluator(BaseResponseEvaluator):

    def __init__(self, sentence, threshold=0.8, call_back=None):
        super().__init__(sentence)
        self.call_back = call_back
        self.threshold = threshold
        self.expected_words = sentence.lower().replace(',', '').replace('.', '').split()

    def evaluate(self, speech_record: messages.SpeechRecorded, publish_queue, proc_name):
        words = speech_record.text.lower().split()
        match_count = 0
        for idx, word in enumerate(words):
            if idx < len(self.expected_words):
                if word == self.expected_words[idx]:
                    match_count += 1
            else:
                if match_count > 0:
                    match_count -= 1
        value = match_count / len(self.expected_words)
        if value >= self.threshold:
            if self.call_back:
                self.call_back(speech_record.text, publish_queue, proc_name)
            return speech_record.text
        return None


class CommandEvaluator(BaseResponseEvaluator):

    def __init__(self, command, threshold=0.8, call_back=None):
        super().__init__(command)
        self.call_back = call_back
        self.threshold = threshold

    def evaluate(self, speech_record: messages.SpeechRecorded, publish_queue, proc_name):
        text = speech_record.text
        if text.startswith(self.text):
            if self.call_back:
                self.call_back(text, publish_queue, proc_name)
            return text
        return None


class CommandEvaluationsDispatcher(object):

    def __init__(self, commands: [], no_command_response: messages.Say=None):
        self.commands = commands
        self.no_command_response = no_command_response

    def evaluate(self, speech_record: messages.SpeechRecorded, publish_queue, proc_name):
        assert isinstance(speech_record, messages.SpeechRecorded), \
            f"should be of type {messages.SpeechRecorded} but was {speech_record}"
        for command in self.commands:
            if command.evaluate(speech_record, publish_queue, proc_name):
                logger.info("activate command callback")
                return
        else:
            if self.no_command_response:
                send_message(publish_queue, self.no_command_response, proc_name)


class NodeResponseEvaluator(SentenceEvaluator):

    def __init__(self, sentence, threshold=0.8, next_node: InteractorNode = None):
        """next_node = None means 'no further questions'"""
        super().__init__(sentence, threshold)
        self.next_node = next_node

class ResponseNeverMatch(BaseResponseEvaluator):

    def __init__(self):
        super().__init__("")

    def evaluate(self, speech_record: messages.SpeechRecorded, publish_queue, proc_name):
        return None  # never match anything

class ResponseAnyWordEvaluator(BaseResponseEvaluator):

    def __init__(self, any_word_of_list: list, next_node: InteractorNode = None):
        super().__init__(", ".join(any_word_of_list))
        self.next_node = next_node
        self.any_word = any_word_of_list

    def evaluate(self, speech_record: messages.SpeechRecorded, publish_queue, proc_name):
        words = speech_record.text.split()  # this already trims whitespace
        for expected in self.any_word:
            for word in words:
                if word == expected:
                    return word
        return None

class RecordEvaluator(BaseResponseEvaluator):

    def __init__(self, next_node: InteractorNode = None, call_back=None):
        super().__init__("record")
        self.next_node = next_node
        self.call_back = call_back

    def evaluate(self, speech_record: messages.SpeechRecorded, publish_queue, proc_name):
        self.text = speech_record.text
        if self.call_back:
            self.call_back(speech_record.text, publish_queue, proc_name)
        return self.text


class InteractorNode(object):
    # since there are messages it will be pretty reactive...
    # but you need to keep in mind too
    # that sometimes an oral response in expected (parse the text for keywords!?)
    # and if it does not come in a certain time it might be good to ask again but differently...
    # and if the input is not useful (either garbage or no keyword) then ask again.
    mumbling_repeat = [
        messages.Say("In english please."),
        messages.Say("I didn't get that, please repeat."),
        messages.Say("What does that mean? ", append_input=True),
        messages.Say("Sorry, didn't get that."),
    ]
    timeout_repeat = [
        messages.Say("I didn't hear your response, please provide some input."),
        messages.Say("I can't hear you, check your microphone."),
        messages.Say("Are you sure that the microphone is not muted?"),
    ]

    def __init__(self, node_id, say_message, response_evaluators: [NodeResponseEvaluator],
                 input_fail_next: InteractorNode = None,
                 timeout_in_s: float = None,
                 mumbling_repeat: [messages.Say] = None,
                 timeout_repeat: [messages.Say] = None,
                 endless=True,
                 forward_input=False):
        self.forward_input = forward_input
        self.forwarded_input = None
        self.node_id = node_id
        self.endless = endless
        self.utterance = say_message
        self.response_evaluators = response_evaluators
        self.timeout = timeout_in_s
        self.time = 0
        self.start_time = 0
        self._expect_record = False
        self._speech_id = None
        self._check_timeout = False
        self.input_fail_next = input_fail_next
        self.matched_evaluator = None
        if mumbling_repeat:
            self.mumbling_repeat = list(mumbling_repeat)
        else:
            self.mumbling_repeat = list(InteractorNode.mumbling_repeat)
        self._original_mumbling_repeat = list(self.mumbling_repeat)
        if timeout_repeat:
            self.timeout_repeat = list(timeout_repeat)
        else:
            self.timeout_repeat = list(InteractorNode.timeout_repeat)
        self._original_timeout_repeat = list(self.timeout_repeat)

    def say(self, publisher_queue, proc_name, additional_input=None):
        _utterance = self.utterance
        if self.forwarded_input:
            _utterance = _utterance.clone()
            _utterance.text += ' ' + self.forwarded_input
        if additional_input:
            _utterance = _utterance.clone()
            _utterance.text += ' ' + additional_input

        self._say(publisher_queue, proc_name, _utterance)

        # reset variables to initial state
        self._expect_record = False
        self.timeout_repeat = list(self._original_timeout_repeat)
        self.mumbling_repeat = list(self._original_mumbling_repeat)
        self.matched_evaluator = None

    def _say(self, publisher_queue, proc_name, msg: messages.Say):
        if msg.speech_id is None:
            msg.speech_id = "ab09809"
        send_message(publisher_queue, msg, proc_name)
        self._speech_id = msg.speech_id
        self._check_timeout = False

    def update(self, publisher_queue, proc_name, dt):
        self.time += dt
        if self.timeout and self._check_timeout:
            if self.time - self.start_time > self.timeout:
                if self.timeout_repeat:
                    msg = self.timeout_repeat.pop(0)
                    self._say(publisher_queue, proc_name, msg)
                    # if self.endless:
                    self.timeout_repeat.append(msg)
                    random.shuffle(self.timeout_repeat)
                else:
                    # todo fail the entire interaction?
                    logger.error("no more timeout repeats! %s", self.utterance.text)
                    pass

    def evaluate(self, publisher_queue, proc_name, speech_recorded: messages.BaseMessage):
        if isinstance(speech_recorded, messages.SpeechRecorded):
            if not self._expect_record:
                return False
            if not self.response_evaluators:  # this way one can add a last word the same way
                return True
            matches = [e for e in self.response_evaluators if e.evaluate(speech_recorded, publisher_queue, proc_name)]
            if matches:
                # match
                self.matched_evaluator = matches[0]  # use first match
                if self.forward_input:
                    if self.matched_evaluator.next_node:
                        self.matched_evaluator.next_node.forwarded_input = self.matched_evaluator.text
                return True
            else:
                if self.mumbling_repeat:
                    msg = self.mumbling_repeat.pop(0)
                    _msg_original_text = msg.text
                    if msg.append_input:
                        msg = msg.clone()
                        msg.text += ' ' + speech_recorded.text  # this accumulates changes... -> original text!
                    self._say(publisher_queue, proc_name, msg)
                    if self.endless:
                        self.mumbling_repeat.append(msg)
                        random.shuffle(self.mumbling_repeat)
                    msg.text = _msg_original_text
                else:
                    logger.warning("no more mumbling repeats! %s", self.utterance.text)
                    self.input_fail_next.next_node = self.input_fail_next  # hack
                    self.matched_evaluator = self.input_fail_next
                    return True  # True here is a hack
        elif isinstance(speech_recorded, messages.SpeechEnded):
            if speech_recorded.speech_id == self._speech_id:
                # start the timeout
                self._check_timeout = True
                self.start_time = self.time
                self._expect_record = True

        return False

class InteractorWaitNode(InteractorNode):

    def __init__(self, node_id, say_message, timeout_in_s, next_node):
        super().__init__(node_id, say_message, [], None, timeout_in_s, None, None, False, None)
        self.time_elapsed = False
        self.next_node = next_node

    def evaluate(self, publisher_queue, proc_name, speech_recorded: messages.BaseMessage):
        if isinstance(speech_recorded, messages.SpeechEnded):
            if speech_recorded.speech_id == self._speech_id:
                # start the timeout
                self._check_timeout = True
                self.start_time = self.time
                self._expect_record = True
        if self.time_elapsed:
            self.matched_evaluator = self
            return True
        return False

    def update(self, publisher_queue, proc_name, dt):
        self.time += dt
        if self.timeout and self._check_timeout:
            if self.time - self.start_time > self.timeout:
                self.time_elapsed = True

# _APPMOUSEFOCUS = 1
# _APPINPUTFOCUS = 2
# _APPACTIVE     = 4

def update_focus(e, publisher_queue, proc_name):
    if e.type == pygame.WINDOWENTER:
        send_message(publisher_queue, messages.MouseFocusChanged(True), proc_name)
    elif e.type == pygame.WINDOWLEAVE:
        send_message(publisher_queue, messages.MouseFocusChanged(False), proc_name)
    elif e.type == pygame.WINDOWFOCUSGAINED:
        send_message(publisher_queue, messages.KeyboardFocusChanged(True), proc_name)
    elif e.type == pygame.WINDOWFOCUSLOST:
        send_message(publisher_queue, messages.KeyboardFocusChanged(False), proc_name)
    elif e.type == pygame.WINDOWMINIMIZED:
        send_message(publisher_queue, messages.AppVisible(False), proc_name)
    elif e.type == pygame.WINDOWRESTORED:
        send_message(publisher_queue, messages.AppVisible(True), proc_name)
    # elif e.type == pygame.ACTIVEEVENT:
    #     if e.state & _APPMOUSEFOCUS == _APPMOUSEFOCUS:
    #         # print('mouse focus ' + ('gained' if gain else 'lost'))
    #         send_message(publisher_queue, messages.MouseFocusChanged(e.gain), proc_name)
    #     if e.state & _APPINPUTFOCUS == _APPINPUTFOCUS:
    #         # print('input focus ' + ('gained' if gain else 'lost'))
    #         send_message(publisher_queue, messages.KeyboardFocusChanged(e.gain), proc_name)
    #     if e.state & _APPACTIVE == _APPACTIVE:
    #         # print('app is ' + ('visible' if gain else 'iconified'))
    #         send_message(publisher_queue, messages.AppVisible(e.gain), proc_name)


logger.debug("imported")
