# -*- coding: utf-8 -*-
"""
This it the main module. This is the main entry point of your code (put in the main() method).
"""
from __future__ import division, print_function

import logging

logger = logging.getLogger(__name__)


def main(log_queue, log_level, publisher_queue, consumer_queue, proc_name, *args):
    import getpass
    import os
    import random

    import pygame

    from gamelib import messages, pygametext, shared, featureswitch, textinput
    from gamelib.shared import Settings, get_messages, send_message
    from pyknic import context
    from pyknic.mathematics import Vec2, Point2
    from pyknic.pyknic_pygame.context import PygameInitContext, AppInitContext, TimeSteppedContext
    from pyknic.pyknic_pygame.sfx import MusicPlayer
    from pyknic.pyknic_pygame.spritesystem import Sprite, DefaultRenderer, Camera
    from pyknic.tweening import Tweener



    settings = Settings()
    settings.title = "Roger."  # todo change!?
    settings.path_to_icon = "./data/roger_icon.png"

    # """Interactive Workflows
    #
    # The two workflows are computer-initiated or player-initiated. Both workflows, when they complete, enter into the
    # looping workflow.
    #
    # 1. initiated by computer
    # 1.1 ComputerInitiatedState () -> ComputerGreetState
    # 1.2 ComputerGreetState () -> ComputerWaitState
    #
    # 2. initiated by player
    # 2.1 PlayerInitiatedState () -> ComputerWaitState
    #
    # 3. looping workflow
    # 3.1 ComputerWaitState (answer) -> ComputerInterpretState
    # 3.2 ComputerInterpretState (answer -> action) -> ComputerDoState
    # 3.3 ComputerDoState (action) -> ComputerWaitState
    #
    # Note that ComputerDoState (action) can result in:
    # - carrying out a command
    # - giving an answer
    # - asking a question
    # All three actions will advance the dialogue if there is one in progress.
    # """

    # TODO Started integrating states and methods in YogiIntroContext class. The methods still need stt code to work.

    # TODO There is a potential bug here. If the computer initiates command mode, then it requires a response.
    # If it does not get a response, or if the responses are not understood, the computer will sleep. But the problem
    # with sleeping is that the computer forgets what it was doing. This can put the player in an inescapable pit. Some
    # kind of memorization is probably needed.

    #
    # class _BaseState:
    #     def enter(self, dt=0):
    #         pass
    #
    #     def think(self, dt=0):
    #         pass
    #
    #     def exit(self, dt=0):
    #         pass
    #
    #
    # class ComputerInitiateState(_BaseState):
    #     """1.1 Initiated by computer"""
    #
    #     def enter(self, dt=0):
    #         self.answer_pending = True
    #         self.answer_elapsed = 5
    #         self.answer_retry = 0
    #
    #     def think(self, dt=0):
    #         return ComputerGreetState
    #
    #
    # class ComputerGreetState(_BaseState):
    #     """1.2 Greeting by computer"""
    #
    #     def enter(self, dt=0):
    #         self.greeting_done = False
    #
    #     def think(self, dt=0):
    #         if self.greeting_done:
    #             return ComputerWaitState
    #         self.update_greeting(dt)
    #
    #
    # class PlayerInitiateState(_BaseState):
    #     """2.1 Initiated by player"""
    #
    #     def enter(self, dt=0):
    #         self.answer_pending = False
    #
    #     def think(self, dt=0):
    #         return ComputerWaitState
    #
    #
    # class ComputerWaitState(_BaseState):
    #     """3.1 Looping workflow"""
    #
    #     def start(self, dt=0):
    #         self.answer = None
    #
    #     def think(self, dt=0):
    #         if self.answer:
    #             return ComputerInterpretState
    #         if self.answer_pending:
    #             self.update_answer_pending()
    #
    #
    # class ComputerInterpretState(_BaseState):
    #     """3.2 Looping workflow"""
    #
    #     def enter(self, dt=0):
    #         self.action = None
    #
    #     def think(self, dt=0):
    #         self.interpret_answer()
    #         if self.action is not None:
    #             return ComputerDoState
    #
    #
    # class ComputerDoState(_BaseState):
    #     """3.3 Looping workflow"""
    #
    #     def enter(self, dt=0):
    #         self.action_in_progress = True
    #
    #     def think(self, dt=0):
    #         if not self.action_in_progress:
    #             return ComputerWaitState
    #
    #

    class SpriteWithText(Sprite):

        def __init__(self, image, position, anchor, z_layer, text):
            super().__init__(image, position, anchor, z_layer)
            self.is_important = 0
            if text.startswith('!'):
                text = text.lstrip('!')
                self.is_important = -1
            self.text = text
            iw, ih = self.image.get_size()
            self.image_w = iw
            self.image_h = ih
            self.area = pygame.Rect(0, 0, iw, ih)

        @property
        def tween_w(self):
            return self.area.w

        @tween_w.setter
        def tween_w(self, value):
            self.area.w = int(value)

    #
    # class HackedSystemContext(TimeSteppedContext):
    #
    #     def __init__(self, publisher_queue, consumer_queue, proc_name):
    #         super().__init__(settings.max_dt, settings.step_dt, settings.draw_fps)
    #         self.consumer_queue = consumer_queue
    #         self.publisher_queue = publisher_queue
    #         self.proc_name = proc_name
    #         self.screen = None
    #         self.renderer = DefaultRenderer()
    #         self.cam = None
    #         self.tweener = Tweener()
    #
    #     def enter(self):
    #         self.screen = pygame.display.get_surface()
    #         sr = self.screen.get_rect()
    #         self.cam = Camera(sr, Point2(sr.centerx, sr.centery))
    #
    #     def exit(self):
    #         pass
    #
    #     def update_step(self, delta_time, sim_time, *args):
    #         self.tweener.update(delta_time)
    #
    #         for event in pygame.event.get():
    #             if event.type == pygame.VIDEORESIZE:
    #                 pass
    #         for message in get_messages(self.consumer_queue):
    #             if isinstance(message, messages.Quit):
    #                 self.pop(100)
    #
    #     def draw(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0):
    #         self.renderer.draw(self.screen, self.cam, (0, 0, 0), do_flip)
    #
    #     def resume(self):
    #         pass
    #
    #     def suspend(self):
    #         pass
    #
    #
    # class NormalSystemContext(TimeSteppedContext):
    #
    #     def __init__(self, publisher_queue, consumer_queue, proc_name):
    #         super().__init__(settings.max_dt, settings.step_dt, settings.draw_fps)
    #         self.consumer_queue = consumer_queue
    #         self.publisher_queue = publisher_queue
    #         self.proc_name = proc_name
    #         self.screen = None
    #         self.renderer = DefaultRenderer()
    #         self.cam = None
    #         self.tweener = Tweener()
    #
    #     def enter(self):
    #         self.screen = pygame.display.get_surface()
    #         sr = self.screen.get_rect()
    #         self.cam = Camera(sr, Point2(sr.centerx, sr.centery))
    #
    #     def exit(self):
    #         pass
    #
    #     def update_step(self, delta_time, sim_time, *args):
    #         self.tweener.update(delta_time)
    #
    #         for event in pygame.event.get():
    #             if event.type == pygame.VIDEORESIZE:
    #                 pass
    #         for message in get_messages(self.consumer_queue):
    #             if isinstance(message, messages.Quit):
    #                 self.pop(100)
    #
    #     def draw(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0):
    #         self.renderer.draw(self.screen, self.cam, (0, 0, 0), do_flip)
    #
    #     def resume(self):
    #         pass
    #
    #     def suspend(self):
    #         pass

    # class YogiIntroContext(TimeSteppedContext):
    #
    #     def __init__(self, publisher_queue, consumer_queue, proc_name):
    #         super().__init__(settings.max_dt, settings.step_dt, settings.draw_fps)
    #         self.consumer_queue = consumer_queue
    #         self.publisher_queue = publisher_queue
    #         self.proc_name = proc_name
    #
    #         self.answer_pending = True
    #         self.answer_elapsed = 5
    #         self.answer_retry = 0
    #         self.greeting_done = False
    #         self.answer = None
    #
    #         self.state = None
    #         self.set_state(_BaseState)
    #         self.greeting_id = "greeting_id"
    #
    #     def set_state(self, new_state):
    #         if new_state is not self.state:
    #             logger.debug('set_state: {} -> {}', self.state, new_state)
    #             if self.state is not None:
    #                 self.state.exit(self)
    #             self.state = new_state
    #             self.state.enter(self)
    #
    #     def think(self, dt=0):
    #         new_state = self.state.think(self, dt)
    #         if new_state is not None:
    #             self.set_state(new_state)
    #
    #     def update_greeting(self, dt):
    #         """computer speaks greeting until done
    #         Set self.greeting_done = True when done.
    #         """
    #         # speak computer text
    #         self.send_message(messages.Say("greeting", speech_id=self.greeting_id))
    #         # if all computer text is spoken:
    #         # self.greeting_done = True  <- this part is in the message processing
    #
    #     def update_answer_pending(self, dt):
    #         """computer requires an answer
    #         Set self.answer when it is received.
    #         """
    #         if self.answer_pending:
    #             self.answer_elapsed -= dt
    #             if self.answer_elapsed <= 0:
    #                 self.answer_elapsed = 5
    #                 self.answer_retry += 1
    #                 if self.answer_retry == 1:
    #                     self.prompt(repeat=True)
    #                 elif self.answer_retry == 2:
    #                     self.prompt(say="Are you there?")
    #                 elif self.answer_retry == 3:
    #                     self.prompt(say="Sleeping.")
    #         # todo
    #         # self.answer = get_stt()  <- this part is in the message processing too
    #         # if self.answer:
    #         #     self.answer_pending = False
    #
    #     def prompt(self, say=None, repeat=False):
    #         # todo
    #         # if repeat:
    #         #     messages.Say("again. " + self.last_prompt)
    #         # if say:
    #         #     messages.Say(say)
    #         if repeat:
    #             self.send_message(messages.Say("again" + self.last_prompt))
    #         if say:
    #             self.send_message(messages.Say(say))
    #
    #     def interpret_answer(self):
    #         # todo
    #         # if self.answer is not None:
    #         #     interpret self.answer
    #         # if action is decided:
    #         #     set self.action
    #         e = SentenceEvaluator("I eat donuts")  # will match this with 0.8 threshold (means 8 of 10 words have to match)
    #         if e.evaluate(self.answer, self.publisher_queue,
    #                       self.proc_name):  # note self.publisher_queue, self.proc_name only needed if a callback is provided
    #             # decided
    #             # set self.action
    #             pass
    #
    #     def send_message(self, message):
    #         send_message(self.publisher_queue, message, self.proc_name)
    #
    #     def enter(self):
    #         self.send_message(messages.HelpAiSayIntro())
    #
    #     def exit(self):
    #         pass
    #
    #     def update_step(self, delta_time, sim_time, *args):
    #         for event in pygame.event.get():
    #             pass
    #
    #         for message in get_messages(self.consumer_queue):
    #             if isinstance(message, messages.Quit):
    #                 self.pop(100)
    #             elif isinstance(message, messages.CmdClose):
    #                 self.pop()
    #             elif isinstance(message, messages.SpeechEnded):
    #                 if message.speech_id == self.greeting_id:
    #                     self.greeting_done = True
    #             elif isinstance(message, messages.SpeechRecorded):
    #                 self.answer = message
    #                 self.answer_pending = False
    #
    #     def draw(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0):
    #         pass
    #
    #     def resume(self):
    #         pass
    #
    #     def suspend(self):
    #         pass

    the_game = "the game"

    def _on_input(text):
        send_message(publisher_queue, messages.SpeechRecorded(text), proc_name+"textinput")

    def _on_partial_input(text):
        send_message(publisher_queue, messages.PartialSpeechRecorded(text), proc_name+"textinput")


    class ShowContext(TimeSteppedContext):

        def __init__(self, publisher_queue, consumer_queue, proc_name, hacked=False):
            super().__init__(settings.max_dt, settings.step_dt, settings.draw_fps)
            self.text_input = textinput.TextInput(_on_input, _on_partial_input)
            self.consumer_queue = consumer_queue
            self.publisher_queue = publisher_queue
            self.proc_name = proc_name
            self.screen = None
            self.boot_lines = [
                "Booting kernel 5.23.56.7.567",
                "Loading text to speech module: en_US",
                "Loading speech to text module: en_US",
                "!Checking microphone.....      unknown",
                "!>> IMPORTANT: Insure mic is plugged in and ON",
                "!>> IMPORTANT: read the instructions",
                "Starting HELP subsystem....",
                "Starting APPS subsystem.........",
                "!Starting PERSONAL ASSISTANT subsystem",
                "...................................................................",
                "Loading data for login '" + getpass.getuser() + "'....",
                "...................................................................",
                "...................................................................",
                " ...---...---...---... ....... ....................................",
                "............ ...---...---...---... ................................",
                "...........................---...---...---... ................. ...",
                ".................................... ... ....... ---...---...---...",
                "..............................................................ready            .",
            ]
            if featureswitch.skip_intro:
                self.boot_lines = ["."]  # hack to avoid intro
                send_message(publisher_queue, messages.HelpAiDeactivated(), "fake")
            self.boot_sprites = []
            self.renderer = DefaultRenderer()
            self.cam = None
            self.tweener = Tweener()
            self.help_lines = [
                "",
                "Help",
                "====",
                "",
                "Always available commands:",
                "   help: will show this help page, always available",
                "   'Roger hello': activates help ai the voice interface",
                "",
                "Help ai voice commands (once activated):",
                "   goodbye: de-activates the voice interface of the help ai",
                "   up: navigate up",
                "   down: navigate down",
                "   left: navigate to the left",
                "   right: navigate to the right",
                "   repair <file>: tries to repair a file",
                "   open <file|directory>: opens a new document",
                "   close <file|directory>: closes the open document",
                "   power down: powers down the system",
                "",
                "To exit help activate the help ai and say: 'close help'",
            ]

            self.directories = {
                "home": ["photos", "games", "messages", "help"],
                "photos": ["picture one", "picture two", "picture three"],
                "messages": ["message one"],
                "games": [the_game],
            }

            self.current_dir = "home"
            self.prev_dir = "home"
            self.dir_sprites = {
                "home": [],
                "photos": [],
                "messages": [],
                "help": [],
            }

            self.game_repaired = False
            self.end_speech_id = "end pygamemain"
            self.ai_active_spr = None
            self.ai_inactive_spr = None

            self.hints = [
                messages.Say(
                    "Why give up? Hint: try to open the menu entries like 'open messages'. Read the help by saying 'Help!'",
                    interrupt=True),
                messages.Say("Hint: Check bottom left if your input has been recognised correctly.", interrupt=True),
                messages.Say("Hint: Activate the help ai by saying 'roger hello'", interrupt=True),
                messages.Say("Hint: try to repair the entries. Next time the assistance will shut down. YOU LOOSE!",
                             interrupt=True),
            ]

            self._help_is_active = False

        def open_help(self, *args):
            if not self.dir_sprites["help"]:
                # self.help_sprites = self._create_line_sprites(self.help_lines, 10)
                self.dir_sprites["help"] = self._create_line_sprites(self.help_lines, 10)
            # self.navigate_file("help")
            self.renderer.add_sprites(self.dir_sprites["help"])
            self._help_is_active = True

        def close_help(self, *args):
            self.renderer.remove_sprites(self.dir_sprites["help"])
            self._help_is_active = False
            # self.navigate_file(self.prev_dir)
            # self.prev_dir = self.current_dir

        def _create_line_sprites(self, text_lines, layer):
            _sprites = []
            pos = Point2(0, 0)
            for idx, t in enumerate(text_lines):
                if 'ASSISTANT' in t:
                    color = 'hotpink'
                elif t[:1] == '!':
                    color = 'yellow'
                else:
                    color = 'white'
                surf = pygametext.getsurf(t.lstrip('!'), color=color)
                spr = SpriteWithText(surf, pos, 'topleft', layer, t)
                pos = pos + Vec2(0, spr.image_h)
                _sprites.append(spr)
            return _sprites

        def enter(self):
            # pygame.mixer.init()
            if not (featureswitch.skip_intro or featureswitch.skip_boot):
                pygame.mixer.music.load('data/music/Jessica Robo - Selected Works, 2020 - 08 All tricks, No treats.ogg')
                pygame.mixer.music.play()
            self.screen = pygame.display.get_surface()
            sr = self.screen.get_rect()
            self.cam = Camera(sr, Point2(sr.centerx, sr.centery))

            self.boot_sprites = self._create_line_sprites(self.boot_lines, 0)

            prev_tween = None
            for idx, spr in enumerate(self.boot_sprites):
                spr.area.width = 0
                duration = len(spr.text) * (0.01 if idx < 10 else 0.05)
                tween = self.tweener.create_tween_by_end(spr, "tween_w", 0, spr.image_w, duration, do_start=False)
                if prev_tween:
                    prev_tween = prev_tween.next(tween)
                else:
                    prev_tween = tween

            # signal done somehow
            tween.cb = self.done_boot
            prev_tween.start()
            self.renderer.add_sprites(self.boot_sprites)

            img = pygametext.getsurf("Roger: Active", color='green', bold=True)
            pos = Point2(self.cam.rect.right, self.cam.rect.bottom)
            self.ai_active_spr = SpriteWithText(img, pos, 'bottomright', 110, "Active")
            self.ai_active_spr.visible = False
            self.renderer.add_sprite(self.ai_active_spr)

            img = pygametext.getsurf("Roger: Inactive", color='white')
            self.ai_inactive_spr = SpriteWithText(img, pos, 'bottomright', 0, "Inactive")
            self.ai_inactive_spr.visible = False
            self.renderer.add_sprite(self.ai_inactive_spr)

            if featureswitch.skip_boot:
                self.tweener.clear()
                self.done_boot()

        def done_boot(self, *args):
            logger.info("done init")
            self.renderer.remove_sprites(self.boot_sprites)

            if not self.dir_sprites["help"]:
                self.dir_sprites["help"] = self._create_line_sprites(self.help_lines, 10)
                img = pygame.Surface(self.cam.rect.size)
                img.fill((0,0,0))
                bg_spr = Sprite(img, Point2(0, 0), "topleft", z_layer=9)
                self.dir_sprites["help"].append(bg_spr)

            send_message(self.publisher_queue, messages.HelpAiSayIntro(), self.proc_name)
            # self.navigate_file("home")
            destination = "home"
            sprites = self.dir_sprites.get(destination, [])
            if not sprites:
                # generate sprites
                lines = [destination, "=" * len(destination), " "] + \
                        [" " * 8 + t for t in self.directories.get(destination, [])]
                sprites = self._create_line_sprites(lines, 0)
                self.dir_sprites[destination] = sprites
            self.renderer.add_sprites(sprites)
            self.current_dir = destination
            self.prev_dir = destination

        def exit(self):
            pass

        def update_step(self, delta_time, sim_time, *args):
            self.tweener.update(delta_time)

            mx, my = pygame.mouse.get_pos()
            if mx < 550:
                pygame.mouse.set_pos((mx + random.randint(-50, 50), my + + random.randint(-50, 50)))

            for event in pygame.event.get():
                shared.update_focus(event, self.publisher_queue, self.proc_name)
                if event.type == pygame.VIDEORESIZE:
                    pass
                elif event.type == pygame.QUIT:
                    if self.hints:
                        msg = self.hints.pop(0)
                        send_message(self.publisher_queue, msg, self.proc_name)
                    else:
                        send_message(self.publisher_queue, messages.QuitLoose1(), self.proc_name)

                self.text_input.on_event(event)

            for message in get_messages(self.consumer_queue):
                if isinstance(message, messages.Quit):
                    self.pop(100)
                elif isinstance(message, messages.BaseCmdWithText):
                    if isinstance(message, messages.CmdHelp):
                        self.open_help()
                    elif isinstance(message, messages.CmdOpen):
                        d = message.text.replace("open", "").strip()
                        d = d.replace("picture to", "picture two")
                        self.navigate_file(d)
                    elif isinstance(message, messages.CmdClose):
                        if self._help_is_active:
                            self.close_help()
                        else:
                            self.navigate_back()
                    elif isinstance(message, messages.CmdRepair):
                        d = message.text.replace("repair", "").strip()
                        d = d.replace("picture to", "picture two")
                        if d in self.directories.get(self.current_dir, []):
                            send_message(self.publisher_queue, messages.Say(f"Starting repair for {d}"), self.proc_name)
                            if d == the_game:
                                self.game_repaired = True
                                send_message(self.publisher_queue, messages.Say(f"repair done for {d}", volume=0.75),
                                             self.proc_name)
                            else:
                                send_message(self.publisher_queue, messages.Say(f"repair failed for {d}", volume=0.75),
                                             self.proc_name)
                        else:
                            send_message(self.publisher_queue, messages.Say(f"Unknown file {d}"), self.proc_name)
                    else:
                        send_message(self.publisher_queue, messages.Say("Command not implemented"), self.proc_name)
                elif isinstance(message, messages.SpeechRecorded):
                    self._add_command_fade_sprite(message)
                elif isinstance(message, messages.PartialSpeechRecorded):
                    self._add_command_fade_sprite(message)
                elif isinstance(message, messages.SpeechEnded):
                    if message.speech_id == self.end_speech_id:
                        send_message(self.publisher_queue, messages.StartLevel(1), self.proc_name)
                        messages_quit = messages.QuitProcess(self.proc_name)
                        send_message(self.publisher_queue, messages_quit, self.proc_name)
                elif isinstance(message, messages.HelpAiActivated):
                    self.ai_active_spr.visible = True
                    self.ai_inactive_spr.visible = False
                elif isinstance(message, messages.HelpAiDeactivated):
                    self.ai_active_spr.visible = False
                    self.ai_inactive_spr.visible = True

            self._alpha_throb_important()

        def _alpha_throb_important(self):
            """throb boot messages, to get player's attention"""
            for spr in self.boot_sprites:
                if spr.is_important:
                    alpha = spr.alpha
                    if alpha <= 164:
                        spr.is_important = 1
                        alpha = 164
                    elif alpha >= 255:
                        spr.is_important = -1
                        alpha = 255
                    alpha += spr.is_important
                    spr.alpha = alpha

        def draw(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0):
            self.renderer.draw(self.screen, self.cam, (0, 0, 0), do_flip)

        def resume(self):
            logger.info("resume")

        def suspend(self):
            pass

        def _add_command_fade_sprite(self, message):
            text = message.text
            img = pygametext.getsurf(text)
            pos = Point2(self.cam.rect.left, self.cam.rect.bottom)
            spr = SpriteWithText(img, pos, 'bottomleft', 1000, text)

            def remove_spr(s, *args):
                self.renderer.remove_sprite(s)

            tween = self.tweener.create_tween_by_end(spr, "alpha", 255, 0, 0.5, cb_end=remove_spr, cb_args=[spr])
            tween.start()
            self.renderer.add_sprite(spr)

        def navigate_back(self):
            self.renderer.remove_sprites(self.dir_sprites[self.current_dir])
            destination = self.prev_dir
            sprites = self.dir_sprites.get(destination, [])
            if not sprites:
                # generate sprites
                lines = [destination, "=" * len(destination), " "] + \
                        [" " * 8 + t for t in self.directories.get(destination, [])]
                sprites = self._create_line_sprites(lines, 0)
                self.dir_sprites[destination] = sprites
            self.renderer.add_sprites(sprites)
            self.prev_dir = self.current_dir
            self.current_dir = destination

        def navigate_file(self, destination):
            if destination == "help":
                self.open_help()
                return
            force = False  # hack
            if not force:
                options = self.directories.get(self.current_dir, [])
                if not options or destination not in self.directories.get(self.current_dir, []):
                    send_message(self.publisher_queue, messages.Say(f"no such file"), self.proc_name)
                    return

                is_leave = True if (destination in self.directories[
                    self.current_dir] and destination not in self.directories) else False

                if not is_leave and destination not in self.directories:
                    send_message(self.publisher_queue, messages.Say(f"Unknown file {destination}"), self.proc_name)
                    return
                if is_leave:
                    if destination == the_game and self.game_repaired:
                        send_message(self.publisher_queue, messages.Say(f"Loading game."), self.proc_name)
                        err_msg = messages.Say(f"Error.", volume=0.2, rate=50, speech_id=self.end_speech_id)
                        send_message(self.publisher_queue, err_msg, self.proc_name)
                        return
                    send_message(self.publisher_queue, messages.Say(f"File {destination} is corrupt. Try repair."),
                                 self.proc_name)
                    return

            self.renderer.remove_sprites(self.dir_sprites[self.current_dir])

            sprites = self.dir_sprites.get(destination, [])
            if not sprites:
                # generate sprites
                lines = [destination, "=" * len(destination), " "] + \
                        [" " * 8 + t for t in self.directories.get(destination, [])]
                sprites = self._create_line_sprites(lines, 0)
                self.dir_sprites[destination] = sprites
            self.renderer.add_sprites(sprites)
            self.prev_dir = self.current_dir
            self.current_dir = destination

    # put here your code
    os.environ["SDL_VIDEO_CENTERED"] = str(1)

    def _custom_display_init(display_info, driver_info, wm_info):
        accommodated_height = int(display_info.current_h * 0.9)
        if accommodated_height < settings.screen_height:
            logger.info("accommodating height to %s", accommodated_height)
            settings.screen_height = accommodated_height  # accommodate for title bar and task bar ?
        settings.screen_size = settings.screen_width, settings.screen_height
        logger.info("using screen size: %s", settings.screen_size)

        # initialize the screen
        settings.screen = pygame.display.set_mode(settings.screen_size, settings.screen_flags)

        # mixer values to return
        frequency = settings.MIXER_FREQUENCY
        channels = settings.MIXER_CHANNELS
        mixer_size = settings.MIXER_SIZE
        buffer_size = settings.MIXER_BUFFER_SIZE
        # pygame.mixer.pre_init(frequency, mixer_size, channels, buffer_size)
        return frequency, mixer_size, channels, buffer_size

    context.push(PygameInitContext(_custom_display_init))
    music_player = MusicPlayer(settings.master_volume, settings.music_ended_pygame_event)
    context.push(AppInitContext(music_player, settings.path_to_icon, settings.title))

    context.push(ShowContext(publisher_queue, consumer_queue, proc_name))

    context.set_deferred_mode(True)
    while context.length():
        top = context.top()
        if top:
            top.update(0, 0)
        context.update()  # handle deferred context operations

    logger.debug('Finished. Exiting.')


if __name__ == '__main__':
    import queue

    log_queue = None
    log_level = None
    local_publisher_queue = queue.Queue()
    local_consumer_queue = queue.Queue()
    local_proc_name = "local"
    main(log_queue, log_level, local_publisher_queue, local_consumer_queue, local_proc_name)
