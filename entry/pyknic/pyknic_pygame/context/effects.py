# -*- coding: utf-8 -*-

"""
Various graphical effects for transitions between context using pygame.

"""
import logging
import random

import pygame

from pyknic import context
from pyknic import tweening

logger = logging.getLogger(__name__)
logger.debug("importing...")

# slide dx, dy
# slide in
# slide out
# slide from/to any point
# fade

# delete squares
# flip squares
# explode squares
# implode squares
# resolve squares
# fly through squares
# fall down squares

# slide stripe (random)
# jalousie (flip stipe)
# delete stripe

# melt down

# zoom in (and fade)
# fly through

# rotate, one corner fixed, falling image
# rotate outwards, both top corners fixed, split up in middle
# rotate and zoom out/in

# blizzard (turn white for a moment)

# fade using a template


class Slide(context.transitions.TransitionEffect):
    """
    Sliding effect.
    """

    LEFT = (-1, 0)
    RIGHT = (1, 0)
    DOWN = (0, 1)
    UP = (0, -1)

    def __init__(self, duration, distance, direction=LEFT, move_old=True, move_new=True):
        """
        .. todo:: make it an exception if both move_old and move_new are False
        """
        context.transitions.TransitionEffect.__init__(self)
        self.current_time = duration
        dirx, diry = direction
        self.dir_x = dirx
        self.dir_y = diry

        self.posx = 0
        self.posy = 0
        self.speed = distance / duration
        self.distance = distance
        self.duration = duration
        self.move_old = move_old
        self.move_new = move_new
        # TODO:  make this an exception!
        assert move_old or move_new, "moving neither is not allowd"
        self._old_surf = None
        self._new_surf = None

    def enter(self):
        self.current_time = self.duration
        self.posx = 0
        self.posy = 0

    def update(self, delta_time, sim_time):
        self.posx += self.speed * self.dir_x * delta_time
        self.posy += self.speed * self.dir_y * delta_time
        self.current_time -= delta_time
        if self.current_time > 0:
            return False
        return True  # done

    def draw(self, screen, old_context, new_context):
        screen_rect = screen.get_rect()
        old_rect = screen_rect.copy()
        new_rect = screen_rect.copy()

        if not self._old_surf:
            self._old_surf = screen.copy()
        self._old_surf.fill((0, 0, 0, 0))

        if not self._new_surf:
            self._new_surf = screen.copy()
        self._new_surf.fill((0, 0, 0, 0))

        old_context.draw(self._old_surf)
        new_context.draw(self._new_surf)

        if self.move_old and not self.move_new:
            screen.blit(self._new_surf, new_rect)
            screen.blit(self._old_surf, old_rect.move((self.posx, self.posy)))
        else:
            if self.move_old:
                screen.blit(self._old_surf, old_rect.move((self.posx, self.posy)))
            else:
                screen.blit(self._old_surf, old_rect)

            if self.move_new:
                _pos = (self.posx - self.dir_x * new_rect.width, self.posy - self.dir_y * new_rect.height)
                screen.blit(self._new_surf, new_rect.move(_pos))
            else:
                screen.blit(self._new_surf, new_rect)
        pygame.display.flip()

    def __str__(self):
        return "%s(%s, %s, (%s, %s), %s, %s)" % (self.__class__.__name__,
                                                 self.duration,
                                                 self.distance,
                                                 self.dir_x,
                                                 self.dir_y,
                                                 self.move_old,
                                                 self.move_new)


class FadeOutEffect(context.transitions.TransitionEffect):
    # TODO: document fade_in parameter and test it!
    def __init__(self, duration, fade_in=False):  # , resolution):
        self.fade_in = fade_in
        self.duration = duration
        self.current_time = duration
        self.surf = None
        self.alpha_value = 255

    def enter(self):
        self.alpha_value = 255
        self.current_time = self.duration

    def update(self, delta_time, sim_time):
        self.alpha_value = int(255.0 * self.current_time / self.duration)
        if self.fade_in:
            self.alpha_value = 255 - self.alpha_value
        self.alpha_value = 0 if self.alpha_value < 0 else self.alpha_value  # max(0, self.alpha_value)
        self.alpha_value = 255 if self.alpha_value > 255 else self.alpha_value  # min(255, self.alpha_value)

        self.current_time -= delta_time
        if self.current_time > 0:
            return False
        return True  # done

    def draw(self, screen, old_context, new_context):
        if not self.surf:
            self.surf = screen.copy().convert_alpha()
        new_context.draw(screen)
        self.surf.fill((0, 0, 0, 0))
        old_context.draw(self.surf)
        self.surf.fill((255, 255, 255, self.alpha_value), None, pygame.BLEND_RGBA_MULT)
        screen.blit(self.surf, (0, 0))
        # if not self.surf:
        #     self.surf = screen.copy().convert()
        # new_context.draw(screen)
        # old_context.draw(self.surf)
        # self.surf.set_alpha(self.alpha_value)
        # screen.blit(self.surf, (0, 0))
        pygame.display.flip()

    def __str__(self):
        return "%s(%s)" % (self.__class__.__name__, self.duration)


class ExplosionEffect(context.transitions.TransitionEffect):
    def __init__(self, screen_size):
        self.rect = pygame.Rect((0, 0), screen_size)
        self.surf = pygame.Surface(self.rect.size)
        self.old_surf = pygame.Surface(self.rect.size)
        self.sprites = []
        self.area_width = 25
        self.area_height = 25
        self.seen = False
        self.time = 0
        self.sprites.sort(key=lambda spr: spr.layer)

    def enter(self):
        self.seen = False
        area = pygame.Rect(0, 0, self.area_width, self.area_height)
        cx, cy = self.rect.center
        for x in range(0, self.rect.width, self.area_width):
            for y in range(0, self.rect.height, self.area_height):
                sprite = pygame.sprite.Sprite()
                area.x = x
                area.y = y
                # sprite.rect = pygame.Rect(area)
                sprite.image = self.old_surf.subsurface(area)
                dx = area.centerx - cx * 1.0
                dy = area.centery - cy * 1.0
                dist2 = (dx * dx + dy * dy) * 0.000025
                dist2 = dist2 if dist2 else 1.0
                sprite.vx = dx / dist2
                sprite.vy = dy / dist2
                sprite.x = x
                sprite.y = y
                sprite.layer = 1.0 / dist2
                self.sprites.append(sprite)
        self.sprites.sort(key=lambda spr: spr.layer)
        self.time = 0

    def update(self, dt, sim_time):
        self.time += dt
        pygame.event.pump()
        if self.time > 0.5 and self.seen:
            for sprite in self.sprites:
                sprite.x += sprite.vx * dt
                sprite.y += sprite.vy * dt
        if self.time > 2:
            return True  # end effect
        return False

    def draw(self, screen, old_context, new_context):
        old_context.draw(self.old_surf)
        new_context.draw(self.surf)
        for sprite in self.sprites:
            self.surf.blit(sprite.image, (sprite.x, sprite.y))

        screen.blit(self.surf, (0, 0))
        self.seen = True
        pygame.display.flip()

    def __str__(self):
        return "%s(%s)" % (self.__class__.__name__, self.rect.size)


class DisappearingRectsTransition(context.transitions.TransitionEffect):
    def __init__(self, screen_size):
        self.rect = pygame.Rect((0, 0), screen_size)
        self.surf = pygame.Surface(self.rect.size)
        self.old_surf = pygame.Surface(self.rect.size)
        self.sprites = []
        self.seen = False
        self.time = 0
        self.area_width = 25
        self.area_height = 25

    def enter(self):
        area = pygame.Rect(0, 0, self.area_width, self.area_height)
        for x in range(0, self.rect.width, self.area_width):
            for y in range(0, self.rect.height, self.area_height):
                area.x = x
                area.y = y
                sprite = pygame.sprite.Sprite()
                sprite.image = self.old_surf.subsurface(area)
                sprite.rect = sprite.image.get_rect(topleft=area.topleft)
                self.sprites.append(sprite)
        self.time = 0
        self.seen = False

    def update(self, dt, sim_time):
        self.time += dt
        pygame.event.pump()
        if self.time > 0.2 and self.seen:
            for i in range(40):
                if self.sprites:
                    sprite = random.choice(self.sprites)
                    self.sprites.remove(sprite)

        if not self.sprites:
            return True  # end
        return False

    def draw(self, screen, old_context, new_context):
        old_context.draw(self.old_surf)
        new_context.draw(self.surf)
        for sprite in self.sprites:
            self.surf.blit(sprite.image, sprite.rect)

        screen.blit(self.surf, (0, 0))
        self.seen = True

    def __str__(self):
        return "%s(%s)" % (self.__class__.__name__, self.rect.size)


class UnsupportedDirectionException(Exception):
    pass


class _Meltdown(object):
    def __init__(self, width, height, min_size=10, max_size=100, min_dist=10,
                 min_width=30, width_add=20, finished=50, direction=(0, 1), y_influence=0.1):

        if direction not in [(0, 1), (0, -1), (1, 0), (-1, 0)]:
            raise UnsupportedDirectionException(
                "not supported direction, should be one of (0, 1), (0, -1), (1, 0), (-1, 0)")
        self.direction = direction
        self.WIDTH = width
        self.HEIGHT = height

        # switch height and width if it melts in the x direction
        if self.direction[0] != 0:
            self.WIDTH, self.HEIGHT = self.HEIGHT, self.WIDTH

        self.MIN_SIZE = min_size
        self.MAX_SIZE = max_size

        self.MAX_RND_DIST = min_dist

        self.max_variable_block_width = min_width
        self.min_block_width = width_add

        self.MAX_HEIGHT = self.HEIGHT - self.MIN_SIZE
        self.FINISH_THRESHOLD = finished

        self._max_block_width = self.max_variable_block_width + self.min_block_width
        self._max_appendix = self._max_block_width // 2 * 5
        self._initial_offset = self._max_block_width // 2 * 5
        self._heights = [0] * (self._initial_offset + self.WIDTH + self._max_appendix)

        self._finished = 0
        self.is_done = False
        self.y_influence = y_influence

    def reset(self):
        self._finished = 0
        self._heights = [0] * (self._initial_offset + self.WIDTH + self._max_appendix)
        self.is_done = False

    def step(self):
        # pre-calculations
        width = random.randrange(self.max_variable_block_width) + self.min_block_width

        # don't try to restrict xloc into [self._initial_offset, self._initial_offset + self.WIDTH] -> this leads to
        # and uneven distribution of the melting down parts
        xloc = random.randrange(self._initial_offset + self.WIDTH + self._max_appendix - self._max_block_width)

        yloc = min(self._heights[xloc:xloc + width + 1])
        if yloc >= self.HEIGHT:
            return None

        # calculate block size
        dist = random.randrange(int(round(yloc * self.y_influence)) + self.MAX_RND_DIST)
        size = random.randrange((yloc + self.MIN_SIZE) if (yloc + self.MIN_SIZE) > self.MAX_SIZE else self.MAX_SIZE)

        # define rects for blitting
        if self.direction == (0, 1):
            # top down
            source_rect = (xloc - self._initial_offset, yloc, width, size)
            clear_rect = (xloc - self._initial_offset, yloc, width, dist)
            destination_pos = (xloc - self._initial_offset, yloc + dist)
        elif self.direction == (0, -1):
            # bottom up
            destination_pos = (xloc - self._initial_offset, self.HEIGHT - yloc - dist - size)
            source_rect = (xloc - self._initial_offset, self.HEIGHT - yloc - size, width, size)
            clear_rect = (xloc - self._initial_offset, self.HEIGHT - yloc - dist, width, dist)
        elif self.direction == (1, 0):
            # left right
            destination_pos = (yloc + dist, xloc - self._initial_offset)
            source_rect = (yloc, xloc - self._initial_offset, size, width)
            clear_rect = (yloc, xloc - self._initial_offset, dist, width)
        elif self.direction == (-1, 0):
            # right left
            destination_pos = (self.HEIGHT - yloc - dist - size, xloc - self._initial_offset)
            source_rect = (self.HEIGHT - yloc - size, xloc - self._initial_offset, size, width)
            clear_rect = (self.HEIGHT - yloc - dist, xloc - self._initial_offset, dist, width)
        else:
            raise UnsupportedDirectionException("wrong argument direction {0}".format(str(self.direction)))

        result = (destination_pos, source_rect, clear_rect)
        # logger.info("result: {0}".format(result))

        # postcalculations
        yloc += dist
        # trace the heights of the affected columns
        self._heights[xloc: xloc + width] = [_h if _h > yloc else yloc for _h in self._heights[xloc: xloc + width]]

        # count the finished columns
        if yloc >= self.MAX_HEIGHT:
            _relevant_columns = self._heights[self._initial_offset: self._initial_offset + self.WIDTH]
            self._finished = sum([1 for _h in _relevant_columns if _h >= self.MAX_HEIGHT])

            if self._finished >= (self.WIDTH - self.FINISH_THRESHOLD):
                self.is_done = True

        return result


class Meltdown(context.transitions.TransitionEffect):
    def __init__(self, width, height, min_size=0, max_size=100, min_dist=10, min_width=30, width_add=20, finished=50,
                 direction=(0, 1), y_influence=0.1):
        self._meltdown = _Meltdown(width, height, min_size, max_size, min_dist, min_width, width_add, finished,
                                   direction, y_influence)
        self._result = None
        self.seen = False
        self._image = None
        self._args = (
            width, height, min_size, max_size, min_dist, min_width, width_add, finished, direction, y_influence)

    def enter(self):
        self.seen = False
        self._result = None
        self._meltdown.reset()

    # noinspection PyUnusedLocal
    def update(self, dt, sim_time):
        pygame.event.pump()
        self._result = self._meltdown.step()
        return self._meltdown.is_done

    def draw(self, screen, old_context, new_context):
        if self.seen is False:
            self.seen = True
            self._image = screen.copy().convert()
            new_context.draw(self._image)
            old_context.draw(screen)

        if self._result:
            screen.blit(screen, self._result[0], self._result[1])
            screen.blit(self._image, self._result[2], self._result[2])

    def __str__(self):
        s = "{{0}}({0})".format(", ".join(("{{{0}}}".format(_idx + 1) for _idx in range(len(self._args)))))
        return s.format(self.__class__.__name__, *self._args)


class FlipTiles(context.transitions.TransitionEffect):
    class Tile(object):

        def __init__(self, rect, delay, flip_duration, flip_x=True, flip_y=True, rect_attr_name="center"):
            self.rect = rect
            self.rect_attr_name = rect_attr_name
            self.delay = delay
            self.flip_duration = flip_duration
            self.flip_x = flip_x
            self.flip_y = flip_y

            self.width = rect.width
            self.height = rect.height
            self.flipped = False

    def __init__(self, size, tiles, logger_instance=False, bg_color=(0, 0, 0)):
        self.tweener = tweening.Tweener(logger_instance=logger_instance)
        self.old_image = pygame.Surface(size)
        self.new_image = pygame.Surface(size)
        assert len(tiles) > 0, "there has to be at least one tile"
        self.tiles = tiles
        self.bg_color = bg_color
        self.size = size

    def __str__(self):
        return "{0}({1})".format(self.__class__.__name__, self.size)

    def enter(self):
        for t in self.tiles:
            t.flipped = False
            if t.flip_x:
                self.tweener.create_tween(t, "width", t.width, -t.rect.width, t.flip_duration, delay=t.delay,
                                          cb_end=self.tween_cb)
            if t.flip_y:
                self.tweener.create_tween(t, "height", t.height, -t.rect.height, t.flip_duration, delay=t.delay,
                                          cb_end=self.tween_cb)

    # noinspection PyUnusedLocal
    def tween_cb(self, tween, *args):
        tile = tween.o
        tile.flipped = True
        self.tweener.create_tween(tile, tween.a, 0, -tween.c, tween.d)

    def update(self, dt, sim_time):
        pygame.event.pump()
        self.tweener.update(dt)
        return len(self.tweener.get_all_tweens(self.tweener.TweenStates.active)) == 0

    def draw(self, screen, old_context, new_context):
        old_context.draw(self.old_image)
        new_context.draw(self.new_image)
        screen.fill(self.bg_color)
        for t in self.tiles:
            s = pygame.Surface(t.rect.size)
            if t.flipped:
                s.blit(self.new_image, (0, 0), t.rect)
            else:
                s.blit(self.old_image, (0, 0), t.rect)
            s = pygame.transform.scale(s, (int(t.width), int(t.height)))
            sr = s.get_rect()
            setattr(sr, t.rect_attr_name, getattr(t.rect, t.rect_attr_name))
            screen.blit(s, sr)
        pygame.display.flip()


class DirectionalSweep(FlipTiles):
    def __init__(self, size, flip_duration=0.75, duration=2.0, logger_instance=False, direction=(1, 1), tile_count_x=20,
                 tile_count_y=20, bg_color=(0, 0, 0)):
        """
        The directional sweep class.

        :param size: The size of the screen or the area it should affect
        :param flip_duration: The duration of a flip of a single tile.
        :param duration: The total duration of the effect.
        :param logger_instance: The logger instance.
        :param direction: The direction the sweep should go, e.g. (1,0) or (1,1) etc.
        :param tile_count_x: How many tiles should there be in x direction.
        :param tile_count_y: How many tiles should there be in y direction.
        :param bg_color: The background color, e.g. (r, g, b)
        """
        self._args = (size, flip_duration, duration, logger_instance, direction, tile_count_x, tile_count_y, bg_color)
        if direction not in [(0, 1), (0, -1), (1, 0), (-1, 0), (1, 1), (-1, 1), (-1, -1), (1, -1)]:
            msg = "not supported direction, should be one of " \
                  "(0, 1), (0, -1), (1, 0), (-1, 0), (1, 1), (-1, 1), (-1, -1), (1, -1)"
            raise UnsupportedDirectionException(msg)

        flip_duration /= 2.0
        area_x = size[0] // tile_count_x
        area_y = size[1] // tile_count_y
        tiles = []
        for ty in range(tile_count_y):
            for tx in range(tile_count_x):
                dx = tx if direction[0] > 0 else tile_count_x - tx - 1
                dx = 0 if direction[0] == 0 else dx
                dy = ty if direction[1] > 0 else tile_count_y - ty - 1
                dy = 0 if direction[1] == 0 else dy
                r = pygame.Rect(tx * area_x, ty * area_y, area_x, area_y)
                delay = dx * 1.0 + dy * 1.0
                t = self.Tile(r, delay, flip_duration, flip_x=True, flip_y=True, rect_attr_name="center")
                tiles.append(t)

        # make sure that the effect does not take longer than defined in 'duration'
        # duration = 2 * flip_duration + max_delay   =>  max_delay = duration - 2 * flip_duration
        # max_delay = longest_delay * x  => x = max_delay / longest_delay
        longest_delay = max((_d.delay for _d in tiles))
        max_delay = duration - (2 * flip_duration)
        delay_factor = max_delay / longest_delay

        for t in tiles:
            t.delay *= delay_factor

        FlipTiles.__init__(self, size, tiles, logger_instance, bg_color)

    def __str__(self):
        s = "{{0}}({0})".format(", ".join(("{{{0}}}".format(_idx + 1) for _idx in range(len(self._args)))))
        return s.format(self.__class__.__name__, *self._args)

logger.debug("imported")
