# -*- coding: utf-8 -*-
"""
This contains code that helps to use pyknic with pygame.


There are pygame specific implementations of pyknic base or abstract modules and classes.
Additional modules are provided to help implement pygame games more easily.
"""
from __future__ import division, print_function

import logging

import pygame

from pyknic import info
from pyknic.pyknic_pygame import context
# from pyknic.pyknic_pygame import resource
from pyknic.pyknic_pygame import resource2
from pyknic.pyknic_pygame import spritesystem
from pyknic.pyknic_pygame import surface
from pyknic.pyknic_pygame import transform
from pyknic.pyknic_pygame import sfx

# noinspection PyUnresolvedReferences

logger = logging.getLogger(__name__)

__version__ = "1.0.0.0"
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID (C) 2011"

__copyright__ = "Copyright 2011, pyknic.pygame"
__credits__ = ["DR0ID"]
__license__ = "New BSD license"
__maintainer__ = "DR0ID"


# log info first before importing anything
def log_info(log_separator_width=50):
    """
    Logs the pygame specific information to the logger (using INFO level).

    :param log_separator_width: The width the separating lines should have.
    """
    logger.info("*" * log_separator_width)
    _format_string = "%-" + str(log_separator_width) + "s"
    logger.info(_format_string % ("pyknic.pygame v" + str(__version__) + " (c) DR0ID 2011"))
    logger.info("using pygame version: " + str(pygame.version.ver) + " (SDL: " + str(pygame.get_sdl_version()) + ")")


log_info()

__all__ = [
    "audio",
    "context",
    "resource",
    "spritesystem",
    "surface",
    "transform",
    "sfx"
]

# check pygame version
info.check_version("pygame", (2, 0, 0), tuple(pygame.version.vernum), (2, 1, 0))

# check pyknic version
info.check_version("pyknic", (5, 0, 0, 0), info.version_info, (7, 0, 0, 0))

logger.debug("imported " + "-" * 50)
