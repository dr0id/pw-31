# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This module contains helper methods for logging.

I as an developer want a switch to change the log level in a config file
I as an developer want a switch to change the log level at runtime
I as an developer want to be able to include logging wit minimal performance impact
I as a game designer want to be able to see log information of just one unit (log filtering as possible solution?)
I as an user want to be able to provide log files in case something went wrong (crash, wrong behavior, etc.)

"""
from __future__ import print_function, division
import os
import sys
import logging
import atexit
import pprint
from pyknic import compatibility

logger = logging.getLogger(__name__)
logger.debug("importing...")

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["get_logger_names", "get_logger_parent_tuples", "log_dict", "log_environment", "print_logging_hierarchy"]

# logging string format support extension both '%' formatting and '{}' formatting are supported
try:
    # noinspection PyStatementEffect
    str
    _unicode = True
except NameError:
    _unicode = False

_orig_get_message = logging.LogRecord.getMessage


def _get_format_message(self):
    """
    LogRecord formatting method that supports '%' formatting and '{}' formatting in strings.
    :return: formatted string
    """
    try:
        return _orig_get_message(self)
    except TypeError:
        if not _unicode:  # if no unicode support...
            msg = str(self.msg)
        else:
            msg = self.msg
            if not isinstance(msg, str):
                try:
                    msg = str(self.msg)
                except UnicodeError:
                    msg = self.msg  # Defer encoding till later
        if self.args:
            args = self.args
            msg = msg.format(*args)
        return msg


logging.LogRecord.getMessage = _get_format_message

# shut down logging properly
atexit.register(logging.shutdown)


def get_logger_names():
    """
    Retrieves the names of all installed loggers.
    :return: Generator of names of the Loggers.
    """
    # noinspection PyUnresolvedReferences
    return (_name for _name, __log in logging.Logger.manager.loggerDict.items() if isinstance(__log, logging.Logger))


def get_logger_parent_tuples():
    """
    Retrieves the (name, parent_name) tuples of all loggers.

    :return: Generator of name tuples.
    """
    _tuple_gen = ((_name, logging.getLogger(_name).parent.name) for _name in get_logger_names())
    for child, parent in _tuple_gen:
        if child == parent:
            raise Exception("found loggers with same name! child: {0} parent: {1}".format(child, parent))
        yield (child, parent)


def print_logging_hierarchy(stream=None):
    """
    Print the logger hierarchy to stream.

    :param stream: The stream to use to write to. If None then sys.stdout is used.
    """
    if stream is None:
        stream = sys.stdout

    class _Node(object):

        def __init__(self, logger_to_use):
            self.logger = logger_to_use
            self.children = []

    _nodes = {}  # {id : _Node}
    _loggers = (__log for __log in logging.Logger.manager.loggerDict.values() if isinstance(__log, logging.Logger))
    for _logger in _loggers:
        _logger_id = id(_logger)
        _parent_logger_id = id(_logger.parent)
        if _logger_id not in _nodes:
            _nodes[_logger_id] = _Node(_logger)
        if _parent_logger_id not in _nodes:
            _nodes[_parent_logger_id] = _Node(_logger.parent)

        _parent_node = _nodes[_parent_logger_id]
        _node = _nodes[_logger_id]
        _parent_node.children.append(_node)

    _root_node = _nodes[id(logging.getLogger())]
    new_line = '\n'
    stream.write("loggers:" + new_line)

    _handlers = set()
    _filters = set()

    _stack = [(_root_node, "    ")]  # stack
    while _stack:
        node, indent = _stack.pop(-1)
        _handlers_info = ["{0}('{1}')".format(_h.__class__.__name__, _h.name) for _h in
                          sorted(node.logger.handlers, key=lambda h: (h.name, id(h)))]
        _handlers |= set(node.logger.handlers)
        _filters_info = ["{0}('{1}')".format(_h.__class__.__name__, _h.name) for _h in
                         sorted(node.logger.filters, key=lambda f: (f.name, id(f)))]
        _filters |= set(node.logger.filters)
        level = logging.getLevelName(node.logger.getEffectiveLevel())
        prop = 1 if node.logger.propagate else 0
        msg = "{0}{1} |{5}|{4}|f: {2} |h: {3}".format(indent, node.logger.name, _filters_info, _handlers_info, level,
                                                      prop)
        stream.write(msg + new_line)
        for child in reversed(sorted(node.children, key=lambda _n: _n.logger.name)):
            _stack.append((child, indent + "    "))

    if _handlers:
        stream.write("handlers:" + new_line)
        for _h in sorted(_handlers, key=lambda f: ((f.name or ""), f.__class__.__name__, len(f.filters))):
            _filter_info = ["{0}('{1}')".format(__h.__class__.__name__, __h.name) for __h in
                            sorted(_h.filters, key=lambda f: (f.name, id(f)))]
            level = logging.getLevelName(_h.level) if hasattr(_h, 'level') else "No attribute 'level'!"
            stream.write("    {0}('{1}')|{3}|f: {2}".format(_h.__class__.__name__, _h.name, _filter_info, level))
            _filters |= set(_h.filters)

    if _filters:
        stream.write("filters:" + new_line)
        for _f in sorted(_filters, key=lambda f: (f.name, f.__class__.__name__)):
            stream.write("    {0}('{1}')".format(_f.__class__.__name__, _f.name))


def log_dict(dict_to_log, title, log_separator_width=50, logger_to_use=None, log_level=logging.INFO):
    """
    Logs a dictionary with log level INFO to the given logger. It will use the pprint module to pretty print the dict.

    :param dict_to_log: The dictionary to log.
    :param title: A title for the dictionary.
    :param log_separator_width: The width of the separating lines
    :param logger_to_use: The logger instance to use. If none, the modules logger instance is used.
    :param log_level: The log level to use when logging the dict entries
    """
    if logger_to_use is None:
        logger_to_use = logger
    logger_to_use.log(log_level, "-" * log_separator_width)
    logger_to_use.log(log_level, title)
    bytes_io = compatibility.StringIO()
    pprint.pprint(dict(dict_to_log), stream=bytes_io)
    formatted_string = bytes_io.getvalue()
    logger_to_use.log(log_level, formatted_string)
    logger_to_use.log(log_level, "-" * log_separator_width)


def log_environment(logger_to_use=None):
    """
    Logs the environment variables with log level INFO to the logger.
    :param logger_to_use: The logger instance to use. If None, then the module loggers instance is used.
    """
    env = os.environ
    log_dict(dict(env), "Environment:", logger_to_use=logger_to_use)


logger.debug("imported")
