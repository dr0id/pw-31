﻿# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'binspace.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Space subdivision algorithm to reduce access time to entities in a certain region in space.
"""
from __future__ import print_function, division

import collections
import logging
from collections import defaultdict

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["SphereBinSpace2", "AabbBinSpace2"]  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


# noinspection PyArgumentList
class _BinSpaceBase(object):
    """
    Base class with common functions.
    """

    def __init__(self, cell_size=50, logger_instance=None):
        """
        Constructor.
        :param cell_size: the size of a single cell. This can be tuned and may have some impact on performance.
        :param logger_instance: if set to None, then the default logger is used, if set to False no logging is done at
            all and if an instance is provided, then this one is used.
        """
        self._cells = collections.defaultdict(set)  # {cell_id: set(entities)}
        self._entity_to_cells = collections.defaultdict(set)  # {entity: set(cells_id)}
        self._cell_size = cell_size
        self._range_cache = dict()  # {(left, right, top, bottom): cell_ids}
        self._logger = self._logger = logger if logger_instance is None else \
            (None if logger_instance is False else logger_instance)
        self._attr = (self._cells, self.cell_size, self._entity_to_cells, self._entity_to_cells.pop, self._range_cache)

    @property
    def cell_size(self):
        return self._cell_size

    @property
    def cell_ids(self):
        return self._cells.keys()

    def add(self, entity):
        """
        Add an entity.
        :param entity: entity to add
        :return: None
        """
        self.adds([entity])

    def adds(self, entities, _int=int, _range=range):
        """
        Adds multiple entities at once.
        :param entities: list of entities.
        :param _int: int function, performance optimization to have the function in local memory
        :param _range: range function, performance optimization to have the function in local memory
        :return: None
        """
        raise NotImplementedError("Override this method.")

    def __len__(self):
        """
        Length operator.
        :return: number of entities within the binspace.
        """
        return len(self._entity_to_cells)

    def clear(self):
        """
        Clears all entities from the binspace.
        :return: None
        """
        self._cells.clear()
        self._entity_to_cells.clear()

    def collides(self, entities, do_self_collision=False, _int=int, _range=range, _set=set, _default_dict=defaultdict):
        """
        Collides the entities with the ones in the binspace.
        :param entities: the entities to check against the binspace.
        :param do_self_collision: should self collision be included in the result, defaults to False.
        :param _int: int function, performance optimization to have the function in local memory
        :param _range: range function, performance optimization to have the function in local memory
        :param _set: set function, performance optimization to have the function in local memory
        :param _default_dict: defaultdict function, performance optimization to have the function in local memory
        :return: a dictionary of { entity: set(others)}
        """
        raise NotImplementedError("Override this method.")

    def get_in_rect(self, x, y, w, h, _int=int, _range=range):
        """
        Returns the entities within or touching the given rect.
        :param x: left position
        :param y: top position
        :param w: width of the rect
        :param h: height of the rect
        :param _int: int function, performance optimization to have the function in local memory
        :param _range: range function, performance optimization to have the function in local memory
        :return: a list of entities
        """
        result = set()
        cx_min = _int(x // self._cell_size)
        cx_max = _int((x + w) // self._cell_size)
        cy_min = _int(y // self._cell_size)
        cy_max = _int((y + h) // self._cell_size)
        self_cells = self._cells

        y_range = _range(cy_min, cy_max + 1)
        for _x in _range(cx_min, cx_max + 1):
            for _y in y_range:
                result |= self_cells[(_x, _y)]

        return list(result)

    def remove(self, entity):
        """
        Remove a entity from the binspace
        :param entity: the entity to remove.
        :return: None
        """
        # code duplication for speed, does the same as removes
        self__cells = self._cells
        for cell in self._entity_to_cells.pop(entity, []):
            # noinspection PyUnresolvedReferences
            self__cells[cell].remove(entity)

    def removes(self, entities):
        """
        Removes the given entities.
        :param entities: list of entities to remove.
        :return: None
        """
        self__entity_to_cells_pop = self._entity_to_cells.pop
        self__cells = self._cells
        for e in entities:
            if e in self._entity_to_cells:
                for cell in self__entity_to_cells_pop(e, []):
                    self__cells[cell].remove(e)


# noinspection PyArgumentList
class SphereBinSpace2(_BinSpaceBase):
    """
    A binspace using a bounding circle.
    """

    def adds(self, entities, _int=int, _range=range):
        """
        Adds multiple entities at once. If an entity is already present, then its removed first (works as update).
        An entity must have following attributes: position (with x and y attributes), radius.
        :param entities: list of entities.
        :param _int: int function, performance optimization to have the function in local memory
        :param _range: range function, performance optimization to have the function in local memory
        :return: None
        """
        self__cells = self._cells
        self__cell_size = self._cell_size
        self__entity_to_cells = self._entity_to_cells
        self__entity_to_cells_pop = self__entity_to_cells.pop
        for ent in entities:
            # c = e, x, y, r
            if ent in self__entity_to_cells:
                # code duplication for speed, does the same as removes
                for cell in self__entity_to_cells_pop(ent, []):
                    self__cells[cell].remove(ent)

            r = _int(ent.radius)
            rr = 2 * r
            cx_min = _int((ent.position.x - r))
            cy_min = _int((ent.position.y - r))

            self_entity_to_cells_e__add = self__entity_to_cells[ent].add

            _left = cx_min // self__cell_size
            _right = (cx_min + rr) // self__cell_size + 1
            _top = cy_min // self__cell_size
            _bottom = (cy_min + rr) // self__cell_size + 1
            key = _left, _right, _top, _bottom
            if key not in self._range_cache:
                x_range = _range(_left, _right)
                y_range = _range(_top, _bottom)
                self._range_cache[key] = [(_x, _y) for _x in x_range for _y in y_range]

            for _cell_id in self._range_cache[key]:
                self__cells[_cell_id].add(ent)
                self_entity_to_cells_e__add(_cell_id)

    def adds_aabb(self, entities, _int=int, _range=range):
        """
        Adds multiple entities at once. If an entity already exists, then its first removed (works as update).
        :param entities: list of entities.
        :param _int: int function, performance optimization to have the function in local memory
        :param _range: range function, performance optimization to have the function in local memory
        :return: None
        """
        self__cells = self._cells
        self__cell_size = self._cell_size
        self__entity_to_cells = self._entity_to_cells
        self__entity_to_cells_pop = self__entity_to_cells.pop
        for e in entities:
            # c = e, x, y, r
            if e in self__entity_to_cells:
                # code duplication for speed, does the same as removes
                for cell in self__entity_to_cells_pop(e, []):
                    self__cells[cell].remove(e)

            r = e.rect
            cx_min = r[0]
            cy_min = r[1]

            self_entity_to_cells_e__add = self__entity_to_cells[e].add

            _left = cx_min // self__cell_size
            _right = (cx_min + r[2]) // self__cell_size + 1
            _top = cy_min // self__cell_size
            _bottom = (cy_min + r[3]) // self__cell_size + 1
            key = _left, _right, _top, _bottom
            if key not in self._range_cache:
                x_range = _range(_left, _right)
                y_range = _range(_top, _bottom)
                self._range_cache[key] = [(_x, _y) for _x in x_range for _y in y_range]

            for _cell_id in self._range_cache[key]:
                self__cells[_cell_id].add(e)
                self_entity_to_cells_e__add(_cell_id)

    def collides(self, entities, do_self_collision=False, _int=int, _range=range, _set=set, _default_dict=defaultdict):
        """
        Collides the entities with the ones in the binspace.
        :param entities: the entities to check against the binspace.
        :param do_self_collision: should self collision be included in the result, defaults to False.
        :param _int: int function, performance optimization to have the function in local memory
        :param _range: range function, performance optimization to have the function in local memory
        :param _set: set function, performance optimization to have the function in local memory
        :param _default_dict: defaultdict function, performance optimization to have the function in local memory
        :return: a dictionary of { entity: set(others)}
        """
        result = _default_dict(_set)
        self_cells = self._cells

        _set_discard = _set.discard
        result_pop = result.pop
        _set_add = _set.add

        others = _set()
        others_clear = others.clear
        self__entity_to_cells = self._entity_to_cells

        for e in entities:

            others_clear()
            result_e = result[e]
            result_e__add = result_e.add

            # in case we already know the entity then we also know its cells already
            if e in self__entity_to_cells:
                for c in self__entity_to_cells[e]:
                    others |= self_cells[c]
            else:
                self__cell_size = self._cell_size
                r = _int(e.radius)
                rr = 2 * r
                cx_min = _int((e.position.x - r))
                cy_min = _int((e.position.y - r))

                _left = cx_min // self__cell_size
                _right = (cx_min + rr) // self__cell_size + 1
                _top = cy_min // self__cell_size
                _bottom = (cy_min + rr) // self__cell_size + 1
                key = _left, _right, _top, _bottom
                if key not in self._range_cache:
                    x_range = _range(_left, _right)
                    y_range = _range(_top, _bottom)
                    self._range_cache[key] = [(_x, _y) for _x in x_range for _y in y_range]

                for _cell_id in self._range_cache[key]:
                    others |= self_cells[_cell_id]

            for other in others:
                if other in result:
                    _set_add(result[other], e)
                else:
                    result_e__add(other)

            if not do_self_collision:
                _set_discard(result_e, e)

            if not result[e]:
                result_pop(e)

        return result


# noinspection PyArgumentList
class AabbBinSpace2(_BinSpaceBase):
    """
    A bin space using an AABB bounding box.
    """

    def __init__(self, cell_size=50, rect_class=None):
        _BinSpaceBase.__init__(self, cell_size=cell_size)
        self._rect_class = rect_class
        if self._rect_class is None:
            import pygame
            self._rect_class = pygame.Rect

    def adds(self, entities, _int=int, _range=range):
        """
        Adds multiple entities at once. If an entity already exists, then its first removed (works as update).
        :param entities: list of entities.
        :param _int: int function, performance optimization to have the function in local memory
        :param _range: range function, performance optimization to have the function in local memory
        :return: None
        """
        # self__cells = self._cells
        # self__cell_size = self._cell_size
        # self__entity_to_cells = self._entity_to_cells
        # self__entity_to_cells_pop = self__entity_to_cells.pop

        # should be a tiny bit faster than the above commented code
        self__cells, self__cell_size, self__entity_to_cells, self__entity_to_cells_pop, self__range_cache = self._attr

        for e in entities:
            # c = e, x, y, r
            if e in self__entity_to_cells:
                # code duplication for speed, does the same as removes
                for cell in self__entity_to_cells_pop(e, []):
                    self__cells[cell].remove(e)

            r = e.aabb
            cx_min = r[0]
            cy_min = r[1]

            self_entity_to_cells_e__add = self__entity_to_cells[e].add

            _left = cx_min // self__cell_size
            _right = (cx_min + r[2]) // self__cell_size + 1
            _top = cy_min // self__cell_size
            _bottom = (cy_min + r[3]) // self__cell_size + 1
            key = _left, _right, _top, _bottom
            if key not in self__range_cache:
                x_range = _range(_left, _right)
                y_range = _range(_top, _bottom)
                self__range_cache[key] = tuple((_x, _y) for _x in x_range for _y in y_range)

            for _cell_id in self__range_cache[key]:
                self__cells[_cell_id].add(e)
                self_entity_to_cells_e__add(_cell_id)

    def collides(self, entities, do_self_collision=False, _int=int, _range=range, _set=set, _default_dict=defaultdict,
                 _set_discard=set.discard, _set_add=set.add):
        """
        Collides the entities with the ones in the binspace.
        :param entities: the entities to check against the binspace.
        :param do_self_collision: should self collision be included in the result, defaults to False.
        :param _int: int function, performance optimization to have the function in local memory
        :param _range: range function, performance optimization to have the function in local memory
        :param _set: set function, performance optimization to have the function in local memory
        :param _default_dict: defaultdict function, performance optimization to have the function in local memory
        :param _set_discard: set.discard, performance optimization to have the function in local memory
        :param _set_add: set.add, performance optimization to have the function in local memory
        :return: a dictionary of { entity: set(others)}
        """
        result = _default_dict(_set)
        result_pop = result.pop

        others = _set()
        others_clear = others.clear

        # self__cells = self._cells
        # self__entity_to_cells = self._entity_to_cells
        # self__cell_size = self._cell_size

        # should be a tiny bit faster than the above commented code
        self__cells, self__cell_size, self__entity_to_cells, self__entity_to_cells_pop, self__range_cache = self._attr

        for ent in entities:

            others_clear()
            result_e = result[ent]
            result_e__add = result_e.add

            # in case we already know the entity then we also know its cells already
            if ent in self__entity_to_cells:
                for c in self__entity_to_cells[ent]:
                    others |= self__cells[c]

            else:
                r = _int(ent.radius)
                rr = 2 * r
                cx_min = _int((ent.position.x - r))
                cy_min = _int((ent.position.y - r))

                _left = cx_min // self__cell_size
                _right = (cx_min + rr) // self__cell_size + 1
                _top = cy_min // self__cell_size
                _bottom = (cy_min + rr) // self__cell_size + 1
                key = _left, _right, _top, _bottom  # todo: maybe the result could be cached with this key directly? invalidate cache as soon something is updated (adds(...))
                if key not in self__range_cache:
                    x_range = _range(_left, _right)
                    y_range = _range(_top, _bottom)
                    cells_hit = [(_x, _y) for _x in x_range for _y in y_range]
                    self__range_cache[key] = cells_hit
                    for _cell_id in cells_hit:
                        others |= self__cells[_cell_id]
                else:

                    for _cell_id in self__range_cache[key]:
                        others |= self__cells[_cell_id]

            if not do_self_collision:
                _set_discard(others, ent)

            for other in others:
                if other in result:
                    _set_add(result[other], ent)
                else:
                    result_e__add(other)

            # if not do_self_collision:
            #     _set_discard(result_e, ent)

            if not result[ent]:
                result_pop(ent)

        return result


logger.debug("imported")
